using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZburseCo.Data;
using ZburseCo.Dtos;
using ZburseCo.Helpers;
using ZburseCo.Models;

namespace ZburseCo.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AdminController : ControllerBase
    {
        private readonly UserManager<User> _userManager;

        private readonly DataContext _context;
        private readonly IRepository _repo;
        private readonly IMapper _mapper;
        public AdminController(DataContext context, UserManager<User> userManager, IRepository repo, IMapper mapper)
        {
            _userManager = userManager;
            _repo = repo;
            _mapper = mapper;
            _context = context;
        }
        [Authorize(Policy = "RequireAdminRole")]
        [HttpGet("getUsers")]
        public async Task<IActionResult> GetUsers([FromQuery] UserParams userParams)
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            //var userFromRepo = await _repo.GetUser(currentUserId, true);

            userParams.UserId = currentUserId;



            var users = await _repo.GetUsers(userParams);

            var usersToReturn = _mapper.Map<IEnumerable<UserForListDto>>(users);

            Response.AddPagination(users.CurrentPage, users.PageSize,
                users.TotalCount, users.TotalPages);
             Response.Headers.Add("x-total-items",users.TotalCount.ToString());

            return Ok(usersToReturn);
        }


        //[Authorize(Policy = "RequireAdminRole")]
        [HttpGet("usersWithRoles")]
        public async Task<IActionResult> GetUsersWithRoles()
        {
            var userList = await (from user in _context.Users
                                  orderby user.UserName
                                  select new
                                  {
                                      Id = user.Id,
                                      UserName = user.UserName,
                                      Roles = (from userRole in user.UserRoles
                                               join role in _context.Roles
                                               on userRole.RoleId
                                               equals role.Id
                                               select role.Name).ToList()
                                  }).ToListAsync();
            return Ok(userList);
        }

        [Authorize(Policy = "RequireAdminRole")]   
        [HttpPost("editRoles/{userName}")]
        public async Task<IActionResult> EditRoles(string userName,RoleEditDto roleEditDto)
        {
            var user=await _userManager.FindByNameAsync(userName);
            var userRoles=await _userManager.GetRolesAsync(user);
            var selectedRoles=roleEditDto.RoleNames;
            selectedRoles=selectedRoles??new string[]{};
            var result=await _userManager.AddToRolesAsync(user,selectedRoles.Except(userRoles));
            if(!result.Succeeded)
            {
                    return BadRequest("Failed to add to roles");

            }

            result=await _userManager.RemoveFromRolesAsync(user,userRoles.Except(selectedRoles));

            if(!result.Succeeded)
            {
                return BadRequest("Failed to remove the roles");
            }
            return Ok(await _userManager.GetRolesAsync(user));
        }


        [Authorize(Policy = "RequireAdminRole")]
        [HttpGet("getOption")]
        public async Task<IActionResult> GetOption()
        {
            var option =await _repo.GetOption();
            var dto = _mapper.Map<OptionDto>(option);
            return Ok(dto);
        }

        [Authorize(Policy = "RequireAdminRole")]
        [HttpPost("updateOption")]
        public async Task<IActionResult> UpdateOption(OptionDto optionDto)
        {
            var option = _mapper.Map<Option>(optionDto);
            var update = _repo.UpdateOption(option);
            if (await _repo.SaveAll())
                return Ok(update);
            return Ok(update);
        }
        [Authorize(Policy = "RequireAdminRole")]

        [HttpPost("addTestimonial")]
        public async Task<IActionResult> AddTestimonial(TestimonialAddDto dto){
            var model=_mapper.Map<Testimonial>(dto);
            _repo.Add(model);
            if(await _repo.SaveAll())
                return Ok(model); 
            return Ok(model);
        }
        [Authorize(Policy = "RequireAdminRole")]

        [HttpPost("editTestimonial")]
        public async Task<IActionResult> EditTestimonial(TestimonialEditDto editDto){
            var model=_mapper.Map<Testimonial>(editDto);
            _repo.Update(model);
            if(await _repo.SaveAll())
                return Ok(model); 
            return Ok(model);
        }
        [Authorize(Policy = "RequireAdminRole")]

        [HttpGet("getAllTestimonials")]
        public async Task<IActionResult>  GetAllTestimonials([FromQuery] UserParams userParams){
            var models=await _repo.GetAllTestimonials(userParams);
             var modelsToReturn = _mapper.Map<IEnumerable<TestimonialForListDto>>(models);
            // Response.AddPagination(models.CurrentPage, models.PageSize,
            //     models.TotalCount, models.TotalPages);
            Response.Headers.Add("x-total-items",models.TotalCount.ToString());
            return Ok(modelsToReturn);
        }
        [Authorize(Policy = "RequireAdminRole")]

        [HttpGet("getTestimonialById/{id}")]
        public async Task<IActionResult> GetTestimonialById(int id){
            var model=await _repo.GetTestimonial(id);
            var returnModel=_mapper.Map<TestimonialEditDto>(model);
            return Ok(returnModel);
        }
        [Authorize(Policy = "RequireAdminRole")]

        [HttpPost("removeTestimonial")]
        public async Task<IActionResult> RemoveTestimonial(TestimonialEditDto dto){
           var model=_mapper.Map<Testimonial>(dto);
            _repo.Delete(model);
            if(await _repo.SaveAll())
                return Ok(true); 
            return Ok(false);
            
        }

        [HttpGet("getUserPayments")]
        public async Task<IActionResult> GetUserPayments([FromQuery] UserParams userParams){
            var payments=await _repo.GetUserPayments(userParams);
             var paymentsToReturn = _mapper.Map<IEnumerable<PaymentDto>>(payments);
            Response.AddPagination(payments.CurrentPage, payments.PageSize,
                payments.TotalCount, payments.TotalPages);
            Response.Headers.Add("x-total-items",payments.TotalCount.ToString());
            return Ok(paymentsToReturn);
        }

    }
}