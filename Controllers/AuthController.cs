using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ZburseCo.Data;
using ZburseCo.Dtos;
using ZburseCo.Models;

namespace ZburseCo.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly DataContext _context;
        private readonly IEmailSender _emailSender;

        public AuthController(IConfiguration config, IMapper mapper,
        UserManager<User> userManager, SignInManager<User> signInManager, DataContext context, IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _emailSender = emailSender;


            _config = config;
            _mapper = mapper;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(UserForRegisterDto userForRegisterDto)
        {

            var userToCreate = _mapper.Map<User>(userForRegisterDto);
            userToCreate.UserName = userToCreate.Email;

            if (_context.Users.Any(wh => wh.Email == userToCreate.Email))
            {
                return BadRequest("Email Already exist");
            }
            var result = await _userManager.CreateAsync(userToCreate, userForRegisterDto.Password);

            var userToReturn = _mapper.Map<UserForDetailedDto>(userToCreate);
            if (result.Succeeded)
            {
                var user = await _userManager.FindByNameAsync(userToCreate.UserName);
                var userRoles = await _userManager.GetRolesAsync(user);
                string[] selectedRoles = { "Member" };

                await _userManager.AddToRolesAsync(user, selectedRoles);

                var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                var confirmationLink = Url.Action("ConfirmEmail", "Auth", new {userId = user.Id, token = token},
                    Request.Scheme);

                var email = userForRegisterDto.Email;

                var subject = "Confirm Email ZBurseCo";

                var message = "<h3>Click on link below to confirm your account on ZBURSE.CO:</h3> <br/><a href="+confirmationLink+">"+confirmationLink+"</a>";
                await _emailSender.SendEmailAsync(email, subject, message);
                // return CreatedAtRoute("GetUser",
                //     new {controller="User", id=userToCreate.Id},userToReturn);
                return Ok();
            }
            return BadRequest(result.Errors);
        }
        [AllowAnonymous]
        [Route("confirmEmail")]

        public async Task<IActionResult> ConfirmEmail(string userId, string token)
        {
            if (userId == null || token == null)
            {
                return RedirectToAction("Index", "Default");
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                
                return RedirectToAction("ConfirmEmail", "Default",new {Error="User Not Found!"});
            }

            var result = await _userManager.ConfirmEmailAsync(user, token);
            if (result.Succeeded)
            {
                return RedirectToAction("ConfirmEmail", "Default", new { Error = "Confirm Email Success full.please Login" });
            }

            return RedirectToAction("Index", "Default");
        }

        [HttpPost("registerLoginProvider")]
        public async Task<IActionResult> RegisterLoginProvider(UserForRegisterDto userForRegisterDto)
        {

            var user = await _userManager.FindByEmailAsync(userForRegisterDto.Email);

            if (user == null)
            {
                var appUser = new User()
                {
                    FirstName = userForRegisterDto.FirstName,
                    LastName = userForRegisterDto.LastName,

                    Email = userForRegisterDto.Email,
                    UserName = userForRegisterDto.Email,

                };

                var result = await _userManager.CreateAsync(appUser, Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(0, 8));
                var userRoles = await _userManager.GetRolesAsync(appUser);
                string[] selectedRoles = { "Member" };

                await _userManager.AddToRolesAsync(appUser, selectedRoles);



                await _context.SaveChangesAsync();
            }

            // generate the jwt for the local user...
            var localUser = await _userManager.FindByNameAsync(userForRegisterDto.Email);
            if (localUser == null)
            {
                return BadRequest("login_failure Failed to create local user account.");
            }


          // await _signInManager.SignInAsync(localUser, isPersistent: false);
            return Ok(new
            {
                token = await GenerateJwtTokenAsync(user),
                user = user
            });
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserForLoginDto userForLoginDto)
        {
            var user = await _userManager.FindByEmailAsync(userForLoginDto.Email);

            if (user==null){
                return BadRequest("User not found!");
            }
            if(!user.EmailConfirmed){
                return BadRequest("Please check and confirm your email.");
            }
            var result = await _signInManager
                .CheckPasswordSignInAsync(user, userForLoginDto.Password, false);

            if (result.Succeeded)
            {
                var appUser = await _userManager.Users.Include(p => p.Photos)
                    .FirstOrDefaultAsync(u => u.NormalizedEmail == userForLoginDto.Email.ToUpper());

                var userToReturn = _mapper.Map<UserForListDto>(appUser);

                return Ok(new
                {
                    token = await GenerateJwtTokenAsync(appUser),
                    user = userToReturn
                });
            }

            return BadRequest("Unauthorized");
            // return BadRequest();
        }


        private async Task<string> GenerateJwtTokenAsync(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier,user.Id.ToString()),
                new Claim(ClaimTypes.Name,user.UserName)
            };

            var roles = await _userManager.GetRolesAsync(user);

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }


            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
            var tokenDescriptor = new SecurityTokenDescriptor
            {

                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
        [HttpGet("SignInWithGoogle")]

        public async Task<IActionResult> HandleExternalLogin()
        {
            var info = await _signInManager.GetExternalLoginInfoAsync();
            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false);

            if (!result.Succeeded) //user does not exist yet
            {
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                var newUser = new User
                {
                    UserName = email,
                    Email = email,
                    EmailConfirmed = true
                };
                var createResult = await _userManager.CreateAsync(newUser);
                if (!createResult.Succeeded)
                    throw new Exception(createResult.Errors.Select(e => e.Description).Aggregate((errors, error) => $"{errors}, {error}"));

                await _userManager.AddLoginAsync(newUser, info);
                // var newUserClaims = info.Principal.Claims.Append(new Claim("userId", newUser.Id));
                // await _userManager.AddClaimsAsync(newUser, newUserClaims);                                
                // await _signInManager.SignInAsync(newUser, isPersistent: false);
                // await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
            }

            return Redirect("http://localhost:5000/dashboard/login");
        }

        [HttpPost("forgetPassword")]
        public async Task<IActionResult> ForgetPassword(ForgotPasswordDto forgotPassword)
        {

            if (ModelState.IsValid)
            {
                // Find the user by email
                var user = await _userManager.FindByEmailAsync(forgotPassword.Email);

                if (user == null)
                {
                    return BadRequest("User not found!");
                }
                // If the user is found AND Email is confirmed
                if ( await _userManager.IsEmailConfirmedAsync(user))
                {
                    // Generate the reset password token
                    var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                    byte[] tokenGeneratedBytes = Encoding.UTF8.GetBytes(token);
                    var codeEncoded = WebEncoders.Base64UrlEncode(tokenGeneratedBytes);
                    // Build the password reset link
                    var passwordResetLink = Url.Action("ResetPasswordLink", "Auth",
                        new { email = forgotPassword.Email, token = codeEncoded }, Request.Scheme);

                    var email = forgotPassword.Email;

                    var subject = "Reset Password ZBurseCo";

                    var message = "<h3>Click on link below to reset password your account on ZBURSE.CO:</h3> <br/><a href=" + passwordResetLink + ">" + passwordResetLink + "</a>";
                    await _emailSender.SendEmailAsync(email, subject, message);
                    return Ok();
                }

                return BadRequest("Email has not been confirmed. Please confirm your Email first.");



            }

            return BadRequest("Email not valid!");
        }


        [AllowAnonymous]
        [Route("resetPassword")]
        public async Task<IActionResult> ResetPasswordLink(string email, string token)
        {
            if (email == null || token == null)
            {
                return RedirectToAction("Index", "Default");
            }

            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {

                return RedirectToAction("ConfirmEmail", "Default", new { Error = "User Not Found!" });
            }

            return Redirect("https://zburse.co/panel/auth/reset-password?email="+email+"&token="+token);
        }
        [HttpPut("resetPass")]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Find the user by email
                var user = await _userManager.FindByEmailAsync(model.Email);

                if (user != null)
                {
                    // reset the user password
                    var codeDecodedBytes = WebEncoders.Base64UrlDecode(model.Token);
                    var codeDecoded = Encoding.UTF8.GetString(codeDecodedBytes);
                    var result = await _userManager.ResetPasswordAsync(user, codeDecoded, model.Password);
                    if (result.Succeeded)
                    {
                        return Ok();
                    }
                    // Display validation errors. For example, password reset token already
                    // used to change the password or password complexity rules not met
                   
                    return BadRequest("Token invalid!");
                }

                // To avoid account enumeration and brute force attacks, don't
                // reveal that the user does not exist
                return BadRequest("User not found!");
            }
            // Display validation errors if model state is not valid
            return BadRequest("password is not valid!");
        }

    }
}