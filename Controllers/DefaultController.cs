using System;
using System.Linq;
using System.Net.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Newtonsoft.Json;
using ZburseCo.Data;
using ZburseCo.Dtos;
using ZburseCo.Models;

namespace ZburseCo.Controllers
{
    [AllowAnonymous]


    public class DefaultController : Controller
    {
      
        public DefaultController()
        {
          
        }
        [AllowAnonymous]
        public ActionResult Index()
        {


         
            return Redirect("Home");
        }

       
        [AllowAnonymous]
        public ActionResult ConfirmEmail(string Error)
        {

            if (Error != null)
            {
                ViewData["Error"] = Error;
            }

            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Payment(Payment payment)
        {
            return Redirect("Payment");
        }
        [AllowAnonymous]
        [HttpPost("CallBack")]
        public ActionResult CallBack(CallbackRequest request)
        {
            // var r = Request.Query;
            return Redirect("Payment");
        }

    }

    public class CallbackRequest
    {
        public string authority { get; set; }
        public string success { get; set; }
    }
}