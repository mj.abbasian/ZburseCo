﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZburseCo.Data;
using ZburseCo.Dtos;
using ZburseCo.Models;

namespace ZburseCo.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class PackageController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly IRepository _repo;

        public PackageController(DataContext context, IMapper mapper, IRepository repo)
        {
            _context = context;
            _mapper = mapper;
            _repo = repo;
        }

        [HttpPost("addPackage")]
        [Authorize(Policy = "RequireAdminRole")]
        public async Task<IActionResult> AddPackage([FromForm] PackageForCreationDto packageForCreationDto)
        {
            var package = _mapper.Map<Package>(packageForCreationDto);
            package.IsActive = true;
            //package.PhotoUrl = packageForCreationDto.File.Name;

            var file = packageForCreationDto.File;

            var basePath = Path.Combine("wwwroot/images");
            var showPath = Path.Combine("images");
            var filePath = "";
            var showFilePath = "";
            if (file.Length > 0)
            {
                var dirName = (DateTime.Now.DayOfYear / 7).ToString();
                if (!Directory.Exists(Path.Combine(basePath, dirName)))
                {
                    Directory.CreateDirectory(Path.Combine(basePath, dirName));
                }
                var fileName = Guid.NewGuid() + ".jpg";
                filePath = Path.Combine(basePath, dirName, fileName);
                showFilePath = Path.Combine(showPath, dirName, fileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }
            package.PhotoUrl = showFilePath;


            _repo.Add(package);

            if (await _repo.SaveAll())
                return NoContent();

            throw new Exception($"Cannot Insert Package");
        }


        [HttpPost("updatePackage")]
        [Authorize(Policy = "RequireAdminRole")]
        public async Task<IActionResult> UpdatePackage([FromForm] PackageForUpdateDto packageForUpdateDto)
        {
            var package = _mapper.Map<Package>(packageForUpdateDto);
          
            if (packageForUpdateDto.File != null)
            {
                var file = packageForUpdateDto.File;

                var basePath = Path.Combine("wwwroot/images");
                var showPath = Path.Combine("images");
                var filePath = "";
                var showFilePath = "";
                if (file.Length > 0)
                {
                    var dirName = (DateTime.Now.DayOfYear / 7).ToString();
                    if (!Directory.Exists(Path.Combine(basePath, dirName)))
                    {
                        Directory.CreateDirectory(Path.Combine(basePath, dirName));
                    }
                    var fileName = Guid.NewGuid() + ".jpg";
                    filePath = Path.Combine(basePath, dirName, fileName);
                    showFilePath = Path.Combine(showPath, dirName, fileName);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }
                }
                package.PhotoUrl = showFilePath;
            }
            


            _repo.Update(package);

            if (await _repo.SaveAll())
                return NoContent();

            throw new Exception($"Cannot Update Package");
        }
        [HttpPost("changeActivePackage/{id}")]
        [Authorize(Policy = "RequireAdminRole")]
        public async Task<IActionResult> ChangeActivePackage(int id)
        {
            var package = await _context.Packages.SingleAsync(wh => wh.Id == id);
            package.IsActive = !package.IsActive;

            if (await _repo.SaveAll())
                return NoContent();

            throw new Exception($"Cannot ChangeActive Package");
        }

        [HttpGet("getPackages")]
        [Authorize(Policy = "RequireAdminRole")]
        public async Task<IActionResult> GetPackages()
        {
            var packages = await _repo.GetPackagesForAdmin();
            var packagesToReturn = _mapper.Map<IEnumerable<PackageForListAdminDto>>(packages);
            return Ok(packagesToReturn);
        }

    }
}
