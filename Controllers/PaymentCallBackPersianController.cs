﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZburseCo.Data;
using ZburseCo.Dtos;

namespace ZburseCo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]

    public class PaymentCallBackPersianController : ControllerBase
    {
        private readonly IRepository _repository;
        private readonly IPaymentService _paymentService;
        public PaymentCallBackPersianController(IRepository repository, IPaymentService paymentService)
        {
            _repository = repository;
            _paymentService = paymentService;
        }

        [Route("paymentCallbackPersian")]
        [AllowAnonymous]
        public async Task<IActionResult> CallBackPersian(string authority, string success)
        {
            // var s=DateTime.Now.Ticks;
            var user = await _repository.GetUserWithAuthority(authority);
            var option = await _repository.GetOption();
            var package =await _repository.GetPackageWithAuthority(authority);
            var paymentOutput = new PaymentOutput()
            {
                Authority = authority,
                Status = success
            };
            var call = await _paymentService.PaymentCallBack(user, paymentOutput, option);
            
            if (await _repository.SaveAll())
            {

                if (call.Status == "0")
                {
                    if (package.Id == 13)
                    {
                        return Redirect("/Payment");

                    }

                    if (package.Id == 17)
                    {
                        return Redirect("/BursOnline");

                    }
                }

                if (call.Status == "100")
                {
                    if (package.Id == 13)
                    {
                        return Redirect("/DownloadPackage?authority=" + authority);

                    }

                    if (package.Id == 17)
                    {
                        return Redirect("/DownloadBursOnline?authority=" + authority);

                    }
                }
                return Ok(call);
            }

            return NoContent();
        }
    }
}
