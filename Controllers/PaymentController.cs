﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ZburseCo.Data;
using ZburseCo.Dtos;
using ZburseCo.Models;

namespace ZburseCo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
   
    public class PaymentController : ControllerBase
    {
        
        private readonly IPaymentService _paymentService;
        private readonly IRepository _repository;

        public PaymentController( IPaymentService paymentService, IRepository repository)
        {
            _paymentService = paymentService;
            _repository = repository;
        }
        [HttpPost("paymentRequest/{userId}/{packageId}")]
        public  async Task<IActionResult> NewRequest(int userId,int packageId)
        {
           // var s=DateTime.Now.Ticks;
            var user =await _repository.GetUser(userId);
            var option = await _repository.GetOption();
            var package = await _repository.GetPackageWithId(packageId);

            if (packageId == 15)
            {
                option.Callback = "http://zburse.co/panel/pages/user/package/binaryOption";
            }

            if (packageId == 18)
            {
                option.Callback = "http://zburse.co/panel/pages/user/package/forex";
            }
            var paymentOutput =await _paymentService.PaymentRequest(user,option,package);
            if (await _repository.SaveAll())
                return Ok(paymentOutput);
            return Ok(paymentOutput);
        }
        [Route("paymentCallback")]
          [AllowAnonymous]
        public async Task<IActionResult> CallBack(PaymentOutput paymentOutput)
        {
            // var s=DateTime.Now.Ticks;
            var user = await _repository.GetUserWithAuthority(paymentOutput.Authority);
            var option = await _repository.GetOption();
          
            var call = await _paymentService.PaymentCallBack(user,paymentOutput, option);
           
            if (await _repository.SaveAll())
                return Ok(call);
            throw new Exception($"Add Payment failed on save");
        }
        [HttpPost("paymentCallbackPersian")]
        [AllowAnonymous]
        public async Task<IActionResult> CallBackPersian(string authority,string success)
        {
            // var s=DateTime.Now.Ticks;
            var user = await _repository.GetUserWithAuthority(authority);
            var option = await _repository.GetOption();
            var paymentOutput=new PaymentOutput()
            {
                Authority = authority,
                Status = success
            };
            var call = await _paymentService.PaymentCallBack(user, paymentOutput, option);
            if (await _repository.SaveAll())
            {


                return Ok(call);
            }

            return NoContent();
        }
    }
}
