﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ZburseCo.Data;
using ZburseCo.Dtos;
using ZburseCo.Helpers;

namespace ZburseCo.Controllers
{
    //[ServiceFilter(typeof(LogUserActivity))]

    [Route("api/[controller]")]
    [ApiController]
    public class PublicController : ControllerBase
    {
        private readonly IRepository _repo;
        private readonly IMapper _mapper;
        public PublicController(IRepository repo, IMapper mapper)
        {
            this._repo = repo;
            this._mapper = mapper;
        }
        [HttpGet("getPackages")]
        public async Task<IActionResult> GetPackages()
        {
            var packages = await _repo.GetPackages();
            var packagesToReturn = _mapper.Map<IEnumerable<PackageForListDto>>(packages);
            return Ok(packagesToReturn);
        }

    }
}
