using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Razor.Language;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ZburseCo.Data;
using ZburseCo.Dtos;
using ZburseCo.Helpers;
using ZburseCo.Models;

namespace ZburseCo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController:ControllerBase
    {
        private readonly IRepository _repo;
        private readonly IMapper _mapper;
        private readonly DataContext _context;
        public UserController(IRepository repo , IMapper mapper, DataContext context)
        {
            this._repo=repo;
            this._mapper = mapper;
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers([FromQuery] UserParams userParams)
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            //var userFromRepo = await _repo.GetUser(currentUserId, true);

            userParams.UserId = currentUserId;

        

            var users = await _repo.GetUsers(userParams);

            var usersToReturn = _mapper.Map<IEnumerable<UserForListDto>>(users);

            Response.AddPagination(users.CurrentPage, users.PageSize,
                users.TotalCount, users.TotalPages);

            return Ok(usersToReturn);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            var user=await _repo.GetUser(id);
            var userToReturn=_mapper.Map<UserForDetailedDto>(user);
            return Ok(userToReturn);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUser(int id, UserForUpdateDto userForUpdateDto)
        {
            if (id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var userFromRepo = await _repo.GetUser(id);
            
            _mapper.Map(userForUpdateDto, userFromRepo);
            userFromRepo.LastActive=DateTime.Now;
            if (await _repo.SaveAll())
                return NoContent();

            throw new Exception($"Updating user {id} failed on save");
        }
        [HttpPost("addNewTicket")]
        public async Task<IActionResult> AddNewTicket(TicketForNewCreationDto ticketForNewCreationDto)
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var user =await _repo.GetUser(currentUserId);
            var model = _mapper.Map<Ticket>(ticketForNewCreationDto);
            model.User = user;
            var tick = DateTime.Now.Ticks.ToString();
            var rnd = new Random();
            var barcode = $"{rnd.Next(1, 9)}{tick.Substring(tick.Length - 5, 5)}";

            model.Number = long.Parse(barcode);
            _repo.Add(model);
            if (await _repo.SaveAll())
                return NoContent();

            throw new Exception($"New Ticket failed on save");
        }
        [HttpGet("getTickets")]
        public async Task<IActionResult> GetTickets([FromQuery] UserParams userParams)
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var roles = User.Claims.Where(wh => wh.Type == ClaimTypes.Role).Select(x => x.Value);

            var option = await _repo.GetOption();
            var roleTicketCategorys = JsonConvert.DeserializeObject<IList<RoleTicketCategory>>(option.RoleTicketCategory);

            var roleTicketCategory = roleTicketCategorys.FirstOrDefault(wh => roles.Contains(wh.Role));
            TicketCategory category = TicketCategory.Profile;
            var admin = false;
            if (roleTicketCategory != null)
            {
                category = roleTicketCategory.Category;
                admin = true;
            }
            //var userFromRepo = await _repo.GetUser(currentUserId, true);

            userParams.UserId = currentUserId;



            var tickets = await _repo.GetTickets(userParams,admin,category);

            var ticketsToReturn = _mapper.Map<IEnumerable<TicketForListDto>>(tickets);


            Response.AddPagination(tickets.CurrentPage, tickets.PageSize,
                tickets.TotalCount, tickets.TotalPages);

            return Ok(ticketsToReturn);
        }
        [HttpGet("getTicketsById/{id}")]
        public async Task<IActionResult> GetTicketsById(int id)
        {
         



            var tickets = await _repo.GetTicketsById(id);

         
            return Ok(tickets);
        }
        [HttpPost("addChildTicket")]
        public async Task<IActionResult> AddChildTicket(TicketForChildCreationDto ticketForChildCreationDto)
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var user = await _repo.GetUser(currentUserId);
            var model = _mapper.Map<Ticket>(ticketForChildCreationDto);
            model.User = user;
            model.LastUpdate=DateTime.Now;
            var parent=_context.Tickets.Single(wh => wh.Id == model.Parent.Id);
            parent.LastUpdate=DateTime.Now;
            if (parent.User == user)
            {
                parent.Status = TicketStatus.UserAnswer;
            }
            else
            {
                parent.Status = TicketStatus.SupportAnswer;
            }
            _repo.Add(model);
            if (await _repo.SaveAll())
                return NoContent();

            throw new Exception($"Child Ticket failed on save");
        }
        [HttpPost("addProduct/{authority}")]
        public async Task<IActionResult> AddProduct(string authority)
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var user = await _repo.GetUser(currentUserId);
            var payment = _context.Payments.Include(p => p.Package).Single(wh => wh.Authority == authority);
            if (payment.Status!="100"){
                return BadRequest("Unsuccessful payment:error 100");
            }
            var model = new Product()
            {

            };
            model.User = user;
            var package =await _repo.GetPackageWithId(payment.Package.Id);
            model.Package = package;
            _repo.Add(model);
            if (await _repo.SaveAll())
                return NoContent();

            return BadRequest($"Add Product failed on save");
        }
         [HttpPost("addProductFree/{packageId}")]
         public async Task<IActionResult> AddProductFree(int PackageId)
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var user = await _repo.GetUser(currentUserId);
         
            var model = new Product()
            {

            };
            model.User = user;
            var package =await _repo.GetPackageWithId(PackageId);
            model.Package = package;
            _repo.Add(model);
            if (await _repo.SaveAll())
                return NoContent();

            return BadRequest($"Add Product failed on save");
        }
        [HttpGet("getProducts")]
        public async Task<IActionResult> GetProducts()
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var products =await _repo.GetProductsByUserId(currentUserId);

            return Ok(products);
        }
    }
}