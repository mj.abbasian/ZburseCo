﻿using System;
using System.Net;
using System.Net.Mail;
using System.Security.Authentication;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;

using ZburseCo.Dtos;

namespace ZburseCo.Data
{
    public class EmailSender: IEmailSender
    {
        private readonly EmailSettings _emailSettings;
        private readonly IHostingEnvironment _env;

        public EmailSender(
            IOptions<EmailSettings> emailSettings,
            IHostingEnvironment env)
        {
            _emailSettings = emailSettings.Value;
            _env = env;
        }
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            try
            {
                //                var mimeMessage=new MimeMessage();
                //
                //                mimeMessage.From.Add(new MailboxAddress(_emailSettings.Sender));
                //
                //                mimeMessage.To.Add(new MailboxAddress(email));
                //
                //                mimeMessage.Subject = subject;
                //
                //                mimeMessage.Body=new TextPart("html")
                //                {
                //                    Text = message
                //                };
                //
                //                using (var client=new SmtpClient())
                //                {
                //                    // client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                //                    //client.SslProtocols = SslProtocols.Ssl3 | SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12 ;
                // //client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                //
                //                    if (_env.IsDevelopment())
                //                    {
                //                        await client.ConnectAsync(_emailSettings.MailServer, _emailSettings.MailPort, MailKit.Security.SecureSocketOptions.StartTls);
                //                    }
                //                    else
                //                    {
                //                        await client.ConnectAsync(_emailSettings.MailServer);
                //                    }
                //                  //  client.AuthenticationMechanisms.Remove("XOAUTH2");
                //                    await client.AuthenticateAsync(_emailSettings.Sender, _emailSettings.Password);
                //
                //                    await client.SendAsync(mimeMessage);
                //
                //                    await client.DisconnectAsync(true);
                //                }
                MailMessage mail = new MailMessage();
                //پارامتر این شی همان حالت معرفی شده در تنظیمات ایمیل سرور می‌باشد که پیشتر معرفی شد.
                SmtpClient smtpServer = new SmtpClient(_emailSettings.MailServer);
                mail.Subject =_emailSettings.SenderName;
                mail.From = new MailAddress(_emailSettings.Sender);
                //ایمیل گیرنده نامه
                mail.To.Add(email);
                //متن نامه
                mail.Body = message;
                mail.IsBodyHtml=true;
                //شماره پورت در اینجا حالت ارسال معمولی و غیر رمز شده مد نظر بوده است
                smtpServer.Port = 25;
                //email address      ,email password
                smtpServer.Credentials = new NetworkCredential(_emailSettings.Sender, _emailSettings.Password);
                smtpServer.EnableSsl = false;
                string userState = "";
                 smtpServer.SendAsync(mail,userState);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.Message);
            }
        }
    }
}