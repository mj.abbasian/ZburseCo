﻿using System.Threading.Tasks;

namespace ZburseCo.Data
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}