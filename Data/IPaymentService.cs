﻿using System.Threading.Tasks;
using ZburseCo.Dtos;
using ZburseCo.Models;

namespace ZburseCo.Data
{
    public interface IPaymentService
    {
        Task<PaymentOutput> PaymentRequest(User user, Option option,Package package);
        Task<PaymentOutput> PaymentCallBack(User user, PaymentOutput paymentOutput, Option option);
    }
}