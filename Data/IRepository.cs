using System.Collections.Generic;
using System.Threading.Tasks;
using ZburseCo.Dtos;
using ZburseCo.Helpers;
using ZburseCo.Models;

namespace ZburseCo.Data
{
    public interface IRepository
    {
         void Add<T> (T entity) where T:class;
         void Delete<T> (T entity) where T:class;
         void Update<T> (T entity) where T:class;
         Task<bool> SaveAll();
         Task<PagedList<User>> GetUsers(UserParams userParams);
         Task<User> GetUser(int id);
         Task<IEnumerable<Package>> GetPackages();
         Task<IEnumerable<Package>> GetPackagesForAdmin();
         Task<Option> GetOption();
         Option UpdateOption(Option optionDto);
         Task<Package> GetPackageWithId(int id);
         Task<User> GetUserWithAuthority(string authority);
         Task<PagedList<Ticket>> GetTickets(UserParams userParams,bool admin,TicketCategory category);
         Task<Ticket> GetTicketsById(int id);
         Task<IEnumerable<Product>> GetProductsByUserId(int userId);
         Task<PagedList<Testimonial>> GetAllTestimonials(UserParams userParams);
         Task<Testimonial> GetTestimonial(int id);
        Task<PagedList<Payment>> GetUserPayments(UserParams userParams);
        Task<Package> GetPackageWithAuthority(string authority);


    }
}