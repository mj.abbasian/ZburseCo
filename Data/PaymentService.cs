﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ZburseCo.Dtos;
using ZburseCo.Models;

namespace ZburseCo.Data
{
    public class PaymentService : IPaymentService
    {
        private const string BaseUrl = "https://gate.yekpay.com/api/payment/";
        private readonly HttpClient _client;
        private readonly IRepository _repository;
        private readonly DataContext _context;

        public PaymentService(HttpClient client, IRepository repository, DataContext context)
        {
            _client = client;
            _repository = repository;
            _context = context;
        }

        public async Task<PaymentOutput> PaymentRequest(User user, Option option, Package package)
        {
            var payment = new Payment()
            {
                User = user,
                Amount = package.Price.ToString(), FromCurrencyCode = option.FromCurrencyCode,
                ToCurrencyCode = option.ToCurrencyCode, OrderId = DateTime.Now.Ticks, FirstName = user.FirstName,
                LastName = user.LastName, Email = user.Email, Mobile = user.PhoneNumber, Address = user.Address,
                PostalCode = user.PostalCode, Country = user.Country, City = user.City, Description = "",
                Package = package
            };
            var model = new
            {
                MerchantId = option.MerchantId, Amount = payment.Amount, FromCurrencyCode = payment.FromCurrencyCode,
                ToCurrencyCode = payment.ToCurrencyCode, OrderNumber = payment.OrderId, Callback = option.Callback,
                FirstName = payment.FirstName, LastName = payment.LastName, Email = payment.Email,
                Mobile = payment.Mobile, Address = payment.Address, PostalCode = payment.PostalCode,
                Country = payment.Country, City = payment.City, Description = payment.Description
            };
            var content = JsonConvert.SerializeObject(model, new JsonSerializerSettings()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
            var httpResponse = await _client.PostAsync(BaseUrl+ "request",
                new StringContent(content, Encoding.Default, "application/json"));

            if (!httpResponse.IsSuccessStatusCode)
            {
                throw new Exception("Cannot add  Payment Request");
            }

            var paymentOutput =
                JsonConvert.DeserializeObject<PaymentOutput>(await httpResponse.Content.ReadAsStringAsync());
            payment.AuthorityCode = paymentOutput.Code;
            payment.AuthorityDescription = paymentOutput.Description;
            payment.Authority = paymentOutput.Authority;
            _repository.Add(payment);

            return paymentOutput;
        }

        public async Task<PaymentOutput> PaymentCallBack(User user, PaymentOutput paymentOutput, Option option)
        {
            var payment = await _context.Payments.SingleAsync(wh => wh.Authority==paymentOutput.Authority);
            payment.Status = paymentOutput.Status;
            var verify=new PaymentOutput();
            verify.Status = paymentOutput.Status;
            verify.Authority = paymentOutput.Authority;
            if (paymentOutput.Status == "1")
            {
                var model = new
                {
                    MerchantId = option.MerchantId,
                    Authority = paymentOutput.Authority
                };
                var content = JsonConvert.SerializeObject(model, new JsonSerializerSettings()
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
                var httpResponse = await _client.PostAsync(BaseUrl+"verify",
                    new StringContent(content, Encoding.Default, "application/json"));

                if (!httpResponse.IsSuccessStatusCode)
                {
                    throw new Exception("Cannot Verify  Payment Request");
                }

                verify =
                    JsonConvert.DeserializeObject<PaymentOutput>(await httpResponse.Content.ReadAsStringAsync());
                payment.VerifyCode = verify.Code;
                payment.VerifyDescription = verify.Description;
                payment.Reference = verify.Reference;
                payment.Status = verify.Code.ToString();
                verify.Status = verify.Code.ToString();
                verify.Authority = paymentOutput.Authority;
            }

            _context.Payments.Update(payment);

            return verify;
        }

        
    }
}