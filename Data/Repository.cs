using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using ZburseCo.Dtos;
using ZburseCo.Helpers;
using ZburseCo.Models;

namespace ZburseCo.Data
{
    public class Repository:IRepository
    {

        private readonly DataContext _context;  
        public Repository(DataContext context)
        {
            this._context=context;
        }

        public void Add<T> (T entity) where T:class 
        {
            _context.Add(entity);
        }

         public void Delete<T> (T entity) where T:class 
        {
            _context.Remove(entity);
        }
         public void Update<T>(T entity) where T : class
         {
             _context.Update(entity);
         }
        public async Task<IEnumerable<Package>> GetPackages()
         {
             var packages = await _context.Packages.AsNoTracking().Where(wh => wh.IsActive).ToListAsync();
             return packages;
         }
         public async Task<IEnumerable<Package>> GetPackagesForAdmin()
         {
             var packages = await _context.Packages.AsNoTracking().ToListAsync();
             return packages;
         }
      
        public async Task<User> GetUser(int id)
        {
            var user=await _context.Users.SingleOrDefaultAsync(u=>u.Id==id);
            return user;
        }

        public async Task<PagedList<Testimonial>> GetAllTestimonials(UserParams userParams){
            var models=_context.Testimonials.AsNoTracking().OrderByDescending(or=>or.Id).AsQueryable();
            return await PagedList<Testimonial>.CreateAsync(models, userParams.PageNumber, userParams.PageSize);

        }

        public async Task<PagedList<User>> GetUsers(UserParams userParams)
        {
            var users = _context.Users.AsNoTracking().Include(r => r.UserRoles)
                .ThenInclude(r => r.Role).OrderByDescending(u => u.LastActive).AsQueryable();

            users = users.Where(u => u.Id != userParams.UserId);

           // users = users.Where(u => u.Gender == userParams.Gender);

          

            if (!string.IsNullOrEmpty(userParams.OrderBy))
            {
                switch (userParams.OrderBy)
                {
                    case "created":
                        users = users.OrderByDescending(u => u.Created);
                        break;
                    default:
                        users = users.OrderByDescending(u => u.LastActive);
                        break;
                }
            }

            return await PagedList<User>.CreateAsync(users, userParams.PageNumber, userParams.PageSize);
        }




        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync()>0;
        }

        public async Task<Option> GetOption()
        {
            var option =await _context.Options.AsNoTracking().FirstAsync();
            return option;
        }
        public  Option UpdateOption(Option optionDto)
        {

             _context.Options.Update(optionDto);

            return optionDto;
        }
        public async Task<Package> GetPackageWithId(int id)
        {
            var package = await _context.Packages.SingleAsync(wh => wh.Id==id);
            return package;
        }
        public async Task<User> GetUserWithAuthority(string authority)
        {
            var user = await _context.Users.AsNoTracking().Include(p => p.Payments).FirstAsync(wh => wh.Payments.Any(w => w.Authority==authority));
            return user;
        }
        public async Task<PagedList<Ticket>> GetTickets(UserParams userParams, bool admin, TicketCategory category)
        {
            var user=await GetUser(userParams.UserId);
            var tickets = _context.Tickets.AsNoTracking().Where(wh => wh.Parent==null).OrderByDescending(u => u.Date).AsQueryable();

            if (admin)
            {
                tickets = tickets.Where(wh => wh.Category == category);
            }
            else
            {
                tickets = tickets.Where(u => u.User == user);

            }

            return await PagedList<Ticket>.CreateAsync(tickets, userParams.PageNumber, userParams.PageSize);
        }
        public async Task<Ticket> GetTicketsById(int id)
        {
           
            var tickets =await _context.Tickets.AsNoTracking()
                .Select(c => new Ticket()
                {
                    Id=c.Id,
                    Title=c.Title,
                    Content = c.Content,
                    Number = c.Number,
                    LastUpdate = c.LastUpdate,
                    Date = c.Date, 
                    Category = c.Category,
                    User = c.User,
                    Childs = c.Childs.Select(u => new Ticket()
                    {
                        Id = u.Id,
                        Title = u.Title,
                        Content = u.Content,
                        Number = u.Number,
                        LastUpdate = u.LastUpdate,
                        Date = u.Date,
                        Category = u.Category,
                        User = u.User

                    }).OrderBy( o => o.LastUpdate).ToList()
                })
                .SingleAsync(wh => wh.Id==id);
          


            return tickets;
        }
        public async Task<IEnumerable<Product>> GetProductsByUserId(int userId)
        {
            var products = await _context.Products.AsNoTracking().Include(u => u.User)
                .Include(p => p.Package).Where(wh => wh.User.Id==userId).ToListAsync();
            return products;
        }

        public async Task<Testimonial> GetTestimonial(int id)
        {
            var model=await _context.Testimonials.SingleOrDefaultAsync(u=>u.Id==id);
            return model;
        }

         public async Task<PagedList<Payment>> GetUserPayments(UserParams userParams)
        {
            var payments=_context.Payments.Include(i=>i.Package).Include(i=>i.User)
                .OrderByDescending(or=>or.CreateDate).AsQueryable();
           
            return await PagedList<Payment>.CreateAsync(payments, userParams.PageNumber, userParams.PageSize);
        }

         public async Task<Package> GetPackageWithAuthority(string authority)
         {
             var payment =await _context.Payments.Include(p => p.Package).SingleOrDefaultAsync(wh => wh.Authority == authority);
            var package=new Package();
             if (payment != null)
             {
                 package = _context.Packages.Single(wh => wh.Id == payment.Package.Id);
             }

             return package;

         }
    }
}