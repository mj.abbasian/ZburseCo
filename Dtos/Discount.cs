﻿using System.Collections.Generic;

namespace ZburseCo.Dtos
{
    public class Discount
    {
        public List<Discs> Discs { get; set; }
       
    }

    public class Discs
    {
        public string Code { get; set; }
        public string Price { get; set; }
    }
}