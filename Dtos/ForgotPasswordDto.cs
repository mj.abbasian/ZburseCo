﻿using System.ComponentModel.DataAnnotations;

namespace ZburseCo.Dtos
{
    public class ForgotPasswordDto
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}