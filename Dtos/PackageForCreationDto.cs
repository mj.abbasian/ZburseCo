﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;

namespace ZburseCo.Dtos
{
    public class PackageForCreationDto
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public string PhotoUrl { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
     
        public IFormFile File { get; set; }
       
    }
}