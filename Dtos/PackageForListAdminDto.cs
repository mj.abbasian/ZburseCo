﻿namespace ZburseCo.Dtos
{
    public class PackageForListAdminDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public string Link { get; set; }
        public string PhotoUrl { get; set; }
        public string Description { get; set; }

        public bool IsActive { get; set; }
    }
}