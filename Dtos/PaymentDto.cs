using System;
namespace ZburseCo.Dtos
{
    public class PaymentDto
    {
        public long Id { get; set; }
        public UserForDetailedDto User { get; set; }
        public PackageForListDto Package { get; set; }
        public string Amount { get; set; }

        public int FromCurrencyCode { get; set; }
        public int ToCurrencyCode { get; set; }
        public long OrderId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public long PostalCode { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Description { get; set; }


        public int AuthorityCode { get; set; }
        public string Authority { get; set; }
        public string AuthorityDescription { get; set; }


        public int VerifyCode { get; set; }
        public string Reference { get; set; }
        public string VerifyDescription { get; set; }
        public string Status { get; set; }
        public DateTime CreateDate { get; set; }

    }
}