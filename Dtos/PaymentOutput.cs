﻿namespace ZburseCo.Dtos
{
    public class PaymentOutput
    {
        public int Code { get; set; }
        public string Description { get; set; }
        public string Authority { get; set; }
        public string Reference { get; set; }
        public string Status { get; set; }
        
    }
}