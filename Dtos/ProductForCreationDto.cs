﻿using System;
using ZburseCo.Models;

namespace ZburseCo.Dtos
{
    public class ProductForCreationDto
    {
      
        public Package Package { get; set; }
        public DateTime CreateDate { get; set; }

        public ProductForCreationDto()
        {
            CreateDate=DateTime.Now;
        }
    }
}