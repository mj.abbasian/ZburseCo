﻿using ZburseCo.Models;

namespace ZburseCo.Dtos
{
    public class ProductForListDto
    {
        public int Id { get; set; }
       
        public User User { get; set; }
        public Package Package { get; set; }
    }
}