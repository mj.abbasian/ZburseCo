﻿using ZburseCo.Helpers;

namespace ZburseCo.Dtos
{
    public class RoleTicketCategory
    {
        public string Role { get; set; }
        public TicketCategory Category { get; set; }
    }
}