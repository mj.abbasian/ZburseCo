namespace ZburseCo.Dtos
{
    public class TestimonialAddDto
    {
        public string Link{set;get;}
        public string Title{set;get;}
        public string Description{set;get;}

    }
}