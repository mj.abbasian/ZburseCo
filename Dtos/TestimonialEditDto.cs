namespace ZburseCo.Dtos
{
    public class TestimonialEditDto
    {
        public int Id{set;get;}
        public string Link{set;get;}
        public string Title{set;get;}
        public string Description{set;get;}
    }
}