﻿using System;
using ZburseCo.Helpers;
using ZburseCo.Models;

namespace ZburseCo.Dtos
{
    public class TicketForChildCreationDto
    {
      public Ticket Parent { get; set; }
        public string Content { get; set; }
  
        public DateTime Date { get; set; }
     

        public TicketForChildCreationDto()
        {
            Date = DateTime.Now;
           
        }
    }
}