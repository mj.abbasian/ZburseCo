﻿using System;
using ZburseCo.Helpers;
using ZburseCo.Models;

namespace ZburseCo.Dtos
{
    public class TicketForListDto
    {
        public int Id { get; set; }
        public Ticket Parent { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public TicketCategory Category { get; set; }
        public long Number { get; set; }
        public DateTime Date { get; set; }
        public DateTime LastUpdate { get; set; }
        public TicketStatus Status { get; set; }
    }
}