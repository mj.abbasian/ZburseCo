﻿using System;
using ZburseCo.Helpers;

namespace ZburseCo.Dtos
{
    public class TicketForNewCreationDto
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public TicketCategory Category { get; set; }
        public long Number { get; set; }
        public DateTime Date { get; set; }
        public DateTime LastUpdate { get; set; }
        public TicketStatus Status { get; set; }

        public TicketForNewCreationDto()
        {
            Date=DateTime.Now;
            LastUpdate=DateTime.Now;
        }
    }
}