using System;
using System.Collections.Generic;
using ZburseCo.Helpers;

namespace ZburseCo.Dtos
{
    public class UserForDetailedDto
    {
         public int Id { get; set; }
        public string Username {  get; set; }
      
        public int Age{set;get;}
        public string KnownAs{set;get;}
      //  public DateTime Created{set;get;}
       // public DateTime LastActive{set;get;}
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
      //  public DateTime DateOfBirth { get; set; }

    }
}