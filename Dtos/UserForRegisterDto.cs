using System;
using System.ComponentModel.DataAnnotations;

namespace ZburseCo.Dtos
{
    public class UserForRegisterDto
    {
        // [Required]
        // public string Username { get; set; }
        [Required]
       
        public string Password { get; set; }

      
        [Required]
        public string Email { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastActive { get; set; }
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public UserForRegisterDto()
        {
            Created = DateTime.Now;
            LastActive = DateTime.Now;
        }
    }
}