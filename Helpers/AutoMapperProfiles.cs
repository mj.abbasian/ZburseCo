using System.Linq;
using AutoMapper;
using ZburseCo.Dtos;
using ZburseCo.Models;

namespace ZburseCo.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, UserForDetailedDto>()
            // .ForMember(dest => dest.PhotoUrl, opt =>
            // {
            //     opt.MapFrom(src => src.Photos.FirstOrDefault(f => f.IsMain).Url);
            // })
            .ForMember(dest => dest.Age, opt =>
            {
                opt.MapFrom(d => d.DateOfBirth.CalculateAge());
            
            })
            .ForMember(dest => dest.Gender, opt =>
            {
                opt.MapFrom(d => (int)d.Gender);

            })
            ;

            CreateMap<User, UserForListDto>()
            // .ForMember(dest => dest.PhotoUrl, opt =>
            // {
            //     opt.MapFrom(src => src.Photos.FirstOrDefault(f => f.IsMain).Url);
            // })
            .ForMember(dest => dest.Age, opt =>
            {
                opt.MapFrom(d => d.DateOfBirth.CalculateAge());
            
            })
            .ForMember(dest => dest.Roles, opt =>
            {
                opt.MapFrom(d => d.UserRoles.Select(x => x.Role.Name));

            });

            CreateMap<Photo, PhotosForDetailedDto>();
            CreateMap<UserForRegisterDto,User>();
             CreateMap<User,UserForRegisterDto>();
             CreateMap<Package, PackageForListDto>();
             CreateMap<Package, PackageForListAdminDto>();
             CreateMap<PackageForCreationDto, Package>();
             CreateMap<PackageForUpdateDto, Package>();
             CreateMap<UserForDetailedDto, User>();
             CreateMap<UserForUpdateDto, User>();
             CreateMap<OptionDto, Option>();
             CreateMap<Option, OptionDto>();
             CreateMap<TicketForNewCreationDto, Ticket>();
             CreateMap<Ticket, TicketForListDto>();
             CreateMap<TicketForChildCreationDto, Ticket>();
             CreateMap<ProductForCreationDto, Product>();
             CreateMap<Product, ProductForListDto>();
             CreateMap<TestimonialAddDto,Testimonial>();
             CreateMap<TestimonialEditDto,Testimonial>();
              CreateMap<Testimonial,TestimonialEditDto>();
              CreateMap<Testimonial,TestimonialForListDto>();
              CreateMap<Payment,PaymentDto>();


        }
    }
}