﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ZburseCo.Migrations
{
    public partial class Option2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CallbackPersian",
                table: "Options",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CallbackPersian",
                table: "Options");
        }
    }
}
