﻿namespace ZburseCo.Models
{
    public class Option
    {
        public int Id { get; set; }

        public string MerchantId { get; set; }
        public string Callback { get; set; }
        public string CallbackPersian { get; set; }
        public int FromCurrencyCode { get; set; }
        public int ToCurrencyCode { get; set; }
        public string RoleTicketCategory { get; set; }
    }
}