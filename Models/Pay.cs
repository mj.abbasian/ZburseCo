﻿using System.ComponentModel.DataAnnotations;

namespace ZburseCo.Models
{
    public class Pay
    {
        [Display(Name = "نام")]
        [Required]
        public string FirstName { get; set; }
        [Display(Name = "نام خانوادگی")]
        [Required]
        public string LastName { get; set; }
        [Display(Name = "ایمیل")]
        [Required]
        public string Email { get; set; }
        [Display(Name = "موبایل")]
        [Required]
        public string Mobile { get; set; }
        [Display(Name = "آدرس")]
        [Required]
        public string Address { get; set; }
        [Display(Name = "کد پستی")]
        [Required]
        public long PostalCode { get; set; }
        [Display(Name = "کشور")]
        [Required]
        public string Country { get; set; }
        [Display(Name = "شهر")]
        [Required]
        public string City { get; set; }
          [Display(Name = "کد تخفیف")]
        
        public string Discount { get; set; }
    }
}