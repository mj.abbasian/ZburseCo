﻿using System;

namespace ZburseCo.Models
{
    public class Product
    {
        public int Id { get; set; }
        public User User { get; set; }
        public Package Package { get; set; }
        public DateTime CreateDate { get; set; }

        public Product()
        {
            CreateDate=DateTime.Now;
        }
    }
}