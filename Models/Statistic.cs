﻿using System;

namespace ZburseCo.Models
{
    public class Statistic
    {
        public long Id { get; set; }
        public string Ip { get; set; }
        public DateTime CreateDate { get; set; }
        public int Count { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
    }
}