﻿using System;
using System.Collections.Generic;
using ZburseCo.Helpers;

namespace ZburseCo.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public User User { get; set; }
        public Ticket Parent { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public TicketCategory Category { get; set; }
        public long Number { get; set; }
        public DateTime Date { get; set; }
        public DateTime LastUpdate { get; set; }
        public TicketStatus Status { get; set; }
        public virtual ICollection<Ticket> Childs { get; set; }


    }
}