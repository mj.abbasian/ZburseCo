using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using ZburseCo.Helpers;

namespace ZburseCo.Models
{
    public class User :IdentityUser<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender{set;get;}
        public DateTime DateOfBirth{set;get;}
        public DateTime Created{set;get;}
        public DateTime LastActive{set;get;}
        public string City{set;get;}
        public string Country{set;get;}
        public string Address{set;get;}
        public long PostalCode{set;get;}
        public ICollection<Photo> Photos{set;get;}
        public ICollection<UserRole> UserRoles{set;get;}
        public virtual ICollection<Payment> Payments { get; set; }

    }
}
