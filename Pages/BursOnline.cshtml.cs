using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using ZburseCo.Data;
using ZburseCo.Dtos;
using ZburseCo.Models;

namespace ZburseCo.Pages
{
    public class BursOnlineModel : PageModel
    {
        private readonly IPaymentService _paymentService;
        private readonly IRepository _repository;
        private readonly DataContext _context;
        private readonly Discount _discount;

        public BursOnlineModel(IPaymentService paymentService, IRepository repository, DataContext context, IOptions<Discount> discount)
        {
            _paymentService = paymentService;
            _repository = repository;
            _context = context;
            _discount = discount.Value;
        }

      

        [BindProperty]
        public Pay Input { get; set; }

        public string ReturnUrl { get; set; }
        public async Task OnGetAsync()
        {
            ViewData["error"] = "";
            var authority = Request.Query["authority"].ToString();
            var success = Request.Query["success"].ToString();
            if (authority.Length > 0)
            {
                var user = await _repository.GetUserWithAuthority(authority);
                var option = await _repository.GetOption();

                var paymentOutput = new PaymentOutput()
                {
                    Authority = authority,
                    Status = success
                };

                var call = await _paymentService.PaymentCallBack(user, paymentOutput, option);
                if (await _repository.SaveAll())
                {
                    if (success == "0")
                    {
                        ViewData["error"] = "Payment canceled";

                    }

                    if (success == "100")
                    {
                        Response.Redirect("/DownloadPackage?authority=" + paymentOutput.Authority);
                    }
                }
            }

        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var user = await _repository.GetUser(1006);
                var option = await _repository.GetOption();
                var package = _context.Packages.First(wh => !wh.IsActive && wh.Id==17);
                var price = package.Price;
                user.FirstName = Input.FirstName;
                user.LastName = Input.LastName;
                user.Email = Input.Email;
                user.PhoneNumber = Input.Mobile;
                user.Address = Input.Address;
                user.PostalCode = Input.PostalCode;
                user.Country = Input.Country;
                user.City = Input.City;

                option.Callback = option.CallbackPersian;
                if (!string.IsNullOrEmpty(Input.Discount))
                {
                    var disc = _discount.Discs.Where(wh => wh.Code == Input.Discount);
                    if (disc.Any())
                    {
                        package.Price = int.Parse(disc.First().Price);
                    }
                }
                var paymentOutput = await _paymentService.PaymentRequest(user, option, package);
                package.Price = price;
                if (await _repository.SaveAll())
                {

                    if (paymentOutput.Code == 100)
                    {
                        Response.Redirect("https://gate.yekpay.com/api/payment/start/" + paymentOutput.Authority);
                    }
                }
            }



            return Page();
        }
    }
}
