﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ZburseCo.Data;

namespace ZburseCo.Pages
{
    public class DownloadModel : PageModel
    {
        private readonly DataContext _context;
        public DownloadModel(DataContext context)
        {
            _context = context;
        }

        public void OnGet()
        {
            ViewData["linkPrimary"] = "";
            ViewData["linkVip"] = "";
            var authority = Request.Query["authority"].ToString();
            if (authority.Length > 0)
            {
                var payment = _context.Payments.Include(p => p.Package).Single(wh => wh.Authority == authority);
                if (payment.AuthorityCode == 100 && payment.Status == "100")
                {
                    var links = payment.Package.Link.Split(',');

                    ViewData["linkPrimary"] = links[0];
                    ViewData["linkVip"] = links[1];
                    ViewData["linkPrimaryForex"] = links[2];

                }
            }
        }
    }
}