using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ZburseCo.Models;
using IEmailSender = ZburseCo.Data.IEmailSender;

namespace ZburseCo.Pages
{
    public class ForgotPasswordModel : PageModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        private readonly UserManager<User> _userManager;
        private readonly Data.IEmailSender _emailSender;

        public ForgotPasswordModel(UserManager<User> userManager, IEmailSender emailSender)
        {
            _userManager = userManager;
            _emailSender = emailSender;
        }

        public void OnGet()
        {
        }
        public async Task<IActionResult> OnPost(string email)
        {
            if (ModelState.IsValid)
            {
                // Find the user by email
                var user = await _userManager.FindByEmailAsync(email);
                await _emailSender.SendEmailAsync(email, "salam", "asasasasas");
                // If the user is found AND Email is confirmed
                if (user != null && await _userManager.IsEmailConfirmedAsync(user))
                {
                    // Generate the reset password token
                    var token = await _userManager.GeneratePasswordResetTokenAsync(user);

                    // Build the password reset link
                    var passwordResetLink = Url.Action("ResetPassword", "Account",
                        new { email = email, token = token }, Request.Scheme);
                  

                    // Log the password reset link
                 

                    // Send the user to Forgot Password Confirmation view
                    return Redirect("ForgotPasswordConfirmation");
                }

                // To avoid account enumeration and brute force attacks, don't
                // reveal that the user does not exist or is not confirmed
                return Redirect("ForgotPasswordConfirmation");
            }

            return Redirect("");
        }
    }
}
