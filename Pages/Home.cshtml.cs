using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ZburseCo.Data;
using ZburseCo.Dtos;
using ZburseCo.Models;

namespace ZburseCo.Pages
{
   
    public class HomeModel : PageModel
    {

        private readonly DataContext _context;
        private readonly IRepository _repository;
        private readonly IActionContextAccessor _accessor;
        public HomeModel(DataContext context, IRepository repository, IActionContextAccessor accessor)
        {
            _context = context;
            _repository = repository;
            _accessor = accessor;
        }
        [BindProperty]
        public IEnumerable<Testimonial> Testimonials { get; set; }
        public async Task OnGetAsync()
        {

            ViewData["Testimonials"] = _context.Testimonials.ToList();
            var ip = _accessor.ActionContext.HttpContext.Connection.RemoteIpAddress.ToString();
            if (ip != "::1")
                await Country(ip);

        }
        public async Task Country(string ip)
        {
            var baseAddress = new Uri("http://ip-api.com/json/"+ip);
            try
            {
                using (var httpClient = new HttpClient { BaseAddress = baseAddress })
                {

                    httpClient.DefaultRequestHeaders.TryAddWithoutValidation("accept", "application/json");

                    using (var response = await httpClient.GetAsync(baseAddress))
                    {

                        string responseData = await response.Content.ReadAsStringAsync();
                        var country = JsonConvert.DeserializeObject<CountryJson>(responseData);
                        var statistic =await _context.Statistics.FirstOrDefaultAsync(wh => wh.Ip == ip);
                        if (statistic != null)
                        {
                            statistic.CreateDate = DateTime.Now;
                            statistic.Count++;
                            await _context.SaveChangesAsync();
                        }
                        else
                        {
                            var model = new Statistic()
                            {
                                CreateDate = DateTime.Now,
                                Ip = ip,
                                Count = 1,
                                Country = country.Country,
                                City = country.City
                            };
                             _repository.Add(model);
                            await _repository.SaveAll();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // ignored
            }
        }

    }
}
