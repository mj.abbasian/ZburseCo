using System;
using System.Net;
using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using ZburseCo.Data;
using ZburseCo.Dtos;
using ZburseCo.Helpers;
using ZburseCo.Models;

namespace ZburseCo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            IdentityBuilder builder = services.AddIdentityCore<User>(opt =>
                    {
                        opt.Password.RequireUppercase = false;
                        opt.Password.RequiredLength = 4;

                        opt.Password.RequireNonAlphanumeric = false;
                        opt.Password.RequireLowercase = false;
                        opt.Password.RequireDigit = false;
                        opt.User.RequireUniqueEmail = true;
                        opt.SignIn.RequireConfirmedEmail = true;
                    })
                    .AddDefaultTokenProviders()
                ;

            builder = new IdentityBuilder(builder.UserType, typeof(Role), builder.Services);
            builder.AddEntityFrameworkStores<DataContext>();
            builder.AddRoleValidator<RoleValidator<Role>>();
            builder.AddRoleManager<RoleManager<Role>>();
            builder.AddSignInManager<SignInManager<User>>();

            // services.AddMvc().AddRazorPagesOptions(options =>
            // {
            //     options.Conventions.ConfigureFilter(new IgnoreAntiforgeryTokenAttribute());
            // });
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddCookie()
                // AddCookie(IdentityConstants.ApplicationScheme, o =>
                // {
                //     o.LoginPath = new PathString("/panel/auth/login");
                //     o.Events = new CookieAuthenticationEvents
                //     {
                //         OnValidatePrincipal = SecurityStampValidator.ValidatePrincipalAsync
                //     };
                // })
                // .AddCookie(IdentityConstants.ExternalScheme, o =>
                // {
                //     o.Cookie.Name = IdentityConstants.ExternalScheme;
                //     o.ExpireTimeSpan = TimeSpan.FromMinutes(5);
                // })
                // .AddCookie(IdentityConstants.TwoFactorRememberMeScheme, o =>
                // {
                //     o.Cookie.Name = IdentityConstants.TwoFactorRememberMeScheme;
                //     o.Events = new CookieAuthenticationEvents
                //     {
                //         OnValidatePrincipal = SecurityStampValidator.ValidateAsync<ITwoFactorSecurityStampValidator>
                //     };
                // })
                // .AddCookie(IdentityConstants.TwoFactorUserIdScheme, o =>
                //     {
                //         o.Cookie.Name = IdentityConstants.TwoFactorUserIdScheme;
                //         o.ExpireTimeSpan = TimeSpan.FromMinutes(5);
                //     })
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey =
                            new SymmetricSecurityKey(
                                Encoding.ASCII.GetBytes(Configuration.GetSection("AppSettings:Token").Value)),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                    };
                })
                ;


            services.AddAuthorization(options =>
            {
                options.AddPolicy("RequireAdminRole", policy => policy.RequireRole("Admin"));
            });

            services.AddMvc(options =>
                {
                    // options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
                    // var policy = new AuthorizationPolicyBuilder()
                    //    .RequireAuthenticatedUser()
                    //    .Build();
                    // options.Filters.Add(new AuthorizeFilter(policy));
                }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(opt =>
                {
                    opt.SerializerSettings.ReferenceLoopHandling =
                        Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                });
            ;
            services.AddCors();
            services.AddAutoMapper(typeof(Startup));
            services.AddScoped<IRepository, Repository>();
            services.AddScoped<IAuthRepository, AuthRepository>();
            // services.AddScoped<LogUserActivity>()
            ;
            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));
            services.Configure<Discount>(Configuration.GetSection("Discounts"));
            services.AddSingleton<IEmailSender, EmailSender>();
            services.AddHttpClient<IPaymentService, PaymentService>();
            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration => { configuration.RootPath = "ngx-admin/dist"; });

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
            //     .CreateScope())
            // {
            //     serviceScope.ServiceProvider.GetService<DataContext>().Database.Migrate();
            // }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(builder =>
                {
                    builder.Run(async context =>
                    {
                        context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;

                        var error = context.Features.Get<IExceptionHandlerFeature>();
                        if (error != null)
                        {
                            context.Response.AddApplicationError(error.Error.Message);
                            await context.Response.WriteAsync(error.Error.Message);
                        }
                    });
                });
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                // app.UseHsts();
            }

            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Default}/{action=Index}");
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ngx-admin";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}