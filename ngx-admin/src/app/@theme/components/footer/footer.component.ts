import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <div class="socials">
      <h6>Contact Us:</h6>
      <a href="mailto:info@Zburse.co" target="_blank" class="ion ion-email"></a>
      <a href="https://t.me/ZBurseSupport" target="_blank" class="ion ion-paper-airplane"></a>
    </div>
    <div class="socials">
    <h6>join and follow us on:</h6>
      <a href="https://www.youtube.com/channel/UCagV8s4TV9sRBCm_Kkdmr1w?disable_polymer=true" target="_blank" class="ion ion-social-youtube"></a>
      <a href="https://www.facebook.com/zburse.forex/" target="_blank" class="ion ion-social-facebook"></a>
      <a href="http://instagram.com/zburse.co" target="_blank" class="ion ion-social-instagram"></a>
      <a href="http://t.me/zburseco" target="_blank" class="ion ion-paper-airplane"></a>
    </div>

  `,
})
export class FooterComponent {
}
