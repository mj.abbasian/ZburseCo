export class Package {
    id: number;
    name: string;
    price: number;
    link: string;
    photoUrl: string;
    description: string;
    isActive: boolean;
    file: any;
}
