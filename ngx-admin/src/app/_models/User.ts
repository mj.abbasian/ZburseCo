import { Photo } from './Photo';

export interface User {
    id: number;
    userName: string;
    age: number;
    gender: string;
    created: Date;
    lastActive: Date;
    city: string;
    country: string;

    address: string;
   dateOfBirth: Date;
    roles: string[];
    firstName: string; lastName: string;
    phoneNumber: string;
    postalCode: string;
}
