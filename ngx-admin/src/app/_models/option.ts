export class Option {
    id: number;
    merchantId: string;
    callback: string;
    fromCurrencyCode: number;
    toCurrencyCode: number;
}
