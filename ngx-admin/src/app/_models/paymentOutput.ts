export class PaymentOutPut {
    code: number;
    description: string;
    authority: string;
    status: string;
    reference: string;
}
