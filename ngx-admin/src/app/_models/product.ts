import { Package } from './Package';

export class Product {
    package: Package;
    id: number;
    createDate: Date;
}
