export class Testimonial {
    public link: string;
    public title: string;
    public description: string;
}
