import { User } from './User';

export class Ticket {
    id: number;
    parent: Ticket;
    title: string;
    content: string;
    category: number;
    number: number;
    date: Date;
    lastUpdate: Date;
    status: number;
    childs: Ticket[];
    user: User;
}
