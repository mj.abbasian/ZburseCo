import {Injectable} from '@angular/core';
import {User} from '../_models/user';
import {Resolve, Router, ActivatedRouteSnapshot} from '@angular/router';
import { UserService } from '../_services/user.service';

import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbComponentStatus, NbToastrService } from '@nebular/theme';


@Injectable()
export class MemberEditResolver implements Resolve<User> {
    userToken: any;
    constructor(private userService: UserService, private router: Router,
         private authService: NbAuthService) {
            this.authService.onTokenChange()
            .subscribe((token: NbAuthJWTToken) => {

              if (token.isValid()) {
                this.userToken = token.getPayload();

              }

            });
        }

    resolve(route: ActivatedRouteSnapshot): Observable<User> {
        return this.userService.getUser(this.userToken.nameid).pipe(
            catchError(error => {
                // this.alertify.error('Problem retrieving your data');
                // this.router.navigate(['/members']);
                 return of(null);
            }),
        );
    }

}
