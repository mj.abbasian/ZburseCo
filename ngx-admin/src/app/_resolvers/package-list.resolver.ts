import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import {Package} from 'app/_models/Package';
import { AdminService } from 'app/_services/admin.service';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
@Injectable()
export class PackageListResolver  implements Resolve <Package[]> {
    constructor(private adminService: AdminService) {}
    resolve(route: ActivatedRouteSnapshot , state: RouterStateSnapshot) {
        return this.adminService.getPackages().pipe(
            catchError(error => {
                // this.alertify.error('Problem retrieving data');
                // this.router.navigate(['/home']);
                return of(null);
            }),
        );
    }
}
