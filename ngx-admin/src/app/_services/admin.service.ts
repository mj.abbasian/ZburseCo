import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Testimonial } from 'app/_models/testimonial';
import {Package} from 'app/_models/Package';
import { Option } from 'app/_models/option';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AdminService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  addTestimonial (model: Testimonial) {
    return this.http.post(this.baseUrl + 'admin/addTestimonial', model);
  }

  removeTestimonial (model: Testimonial) {
    return this.http.post(this.baseUrl + 'admin/removeTestimonial', model);
  }

  editTestimonial (model: Testimonial) {
    return this.http.post(this.baseUrl + 'admin/editTestimonial', model);
  }

  getOption(): Observable<Option> {
    return this.http.get<Option>(this.baseUrl + 'admin/getOption');
  }
  updateOption(option: Option): Observable<Option> {
    return this.http.post<Option>(this.baseUrl + 'admin/updateOption', option);
  }

  addPackage(model) {
    return this.http.post(this.baseUrl + 'package/addPackage', model);
  }
  updatePackage(model) {
    return this.http.post(this.baseUrl + 'package/updatePackage', model);
  }
  changeActivePackage(id) {
    return this.http.post(
      this.baseUrl + 'package/changeActivePackage/' + id,
      {},
    );
  }
  getPackages(): Observable<Package[]> {
    return this.http.get<Package[]>(this.baseUrl + 'package/getPackages');
  }
}

