import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { PaymentOutPut } from '../_models/paymentOutput';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PaymentService {
baseUrl = environment.apiUrl + 'payment';
constructor(private http: HttpClient) { }

paymentRequest(userId: number, packageId: number): Observable<PaymentOutPut> {
  return this.http.post<PaymentOutPut>(this.baseUrl + '/paymentRequest/' + userId + '/' + packageId, {});
}
paymentCallback(paymentOutput: PaymentOutPut): Observable<PaymentOutPut> {
  return this.http.post<PaymentOutPut>(this.baseUrl + '/paymentCallback', paymentOutput);
}
paymentCallbackPersian(paymentOutput: PaymentOutPut): Observable<PaymentOutPut> {
  return this.http.post<PaymentOutPut>(this.baseUrl + '/paymentCallbackPersian', paymentOutput);
}
}
