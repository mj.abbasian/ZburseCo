import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Package } from '../_models/Package';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PublicService {
  baseUrl = environment.apiUrl + 'public';
  constructor(private http: HttpClient) {}

  getPackages(): Observable<Package[]> {
    return this.http.get<Package[]>(this.baseUrl + '/getPackages');
  }
}
