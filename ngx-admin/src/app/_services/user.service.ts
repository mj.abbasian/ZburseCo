import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { User } from '../_models/User';
import { Observable } from 'rxjs';
import { PaginatedResult } from '../_models/pagination';
import { Ticket } from '../_models/ticket';
import { map } from 'rxjs/operators';
import { Product } from '../_models/product';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}
  updateUser(id: number, user: User) {
    return this.http.put(this.baseUrl + 'user/' + id, user);
  }
  getUser(id): Observable<User> {
    return this.http.get<User>(this.baseUrl + 'user/' + id);
  }
  getTickets(
    page?,
    itemsPerPage?,
    userParams?,
  ): Observable<PaginatedResult<Ticket[]>> {
    const paginatedResult: PaginatedResult<Ticket[]> = new PaginatedResult<
      Ticket[]
    >();

    let params = new HttpParams();

    if (page != null && itemsPerPage != null) {
      params = params.append('pageNumber', page);
      params = params.append('pageSize', itemsPerPage);
    }

    return this.http
      .get<Ticket[]>(this.baseUrl + 'user/getTickets', {
        observe: 'response',
        params,
      })
      .pipe(
        map((response) => {
          paginatedResult.result = response.body;
          if (response.headers.get('Pagination') != null) {
            paginatedResult.pagination = JSON.parse(
              response.headers.get('Pagination'),
            );
          }
          return paginatedResult;
        }),
      );
  }
  newTicket(ticket: Ticket): Observable<boolean> {
    return this.http.post<boolean>(this.baseUrl + 'user/addNewTicket', ticket);
  }
  getTicketsById(id): Observable<Ticket> {
    return this.http.get<Ticket>(this.baseUrl + 'user/getTicketsById/' + id);
  }
  addChildTicket(ticket: Ticket): Observable<boolean> {
    return this.http.post<boolean>(this.baseUrl + 'user/addChildTicket', ticket);
  }
  addProduct(authority: string): Observable<boolean> {
    return this.http.post<boolean>(this.baseUrl + 'user/addProduct/' + authority, {});
  }
  addProductFree(packageId: number): Observable<boolean> {
    return this.http.post<boolean>(this.baseUrl + 'user/addProductFree/' + packageId, {});
  }
  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.baseUrl + 'user/getProducts');
  }
}
