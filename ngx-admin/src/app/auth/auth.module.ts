import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgxAuthRoutingModule } from './auth-routing.module';
import { NbAuthModule } from '@nebular/auth';
import { NbAlertModule, NbButtonModule, NbCheckboxModule, NbIconModule, NbInputModule } from '@nebular/theme';


import { NgxRegisterComponent } from './register/register.component';
import { ErrorInterceptorProvider } from 'app/error.interceptor';

import { NgxLoginComponent } from './login/login.component';
import { NgxLogoutComponent } from './ngx-logout/ngx-logout.component';
import { NgxRequestPasswordComponent } from './request-password/request-password.component';
import { NgxResetPasswordComponent } from './reset-password/reset-password.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NbAlertModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    NgxAuthRoutingModule,
    NbIconModule,
    NbAuthModule,
  ],
  declarations: [
    NgxLoginComponent,
    NgxRegisterComponent ,
    NgxLogoutComponent,
    NgxRequestPasswordComponent,
    NgxResetPasswordComponent, // <---
  ],
  providers: [],
})
export class NgxAuthModule {
}
