import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbAuthJWTToken, NbAuthResult, NbAuthService, NbLoginComponent, NbTokenService, NB_AUTH_OPTIONS } from '@nebular/auth';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
})
export class NgxLoginComponent extends NbLoginComponent implements OnInit  {
  returnUrl: string;
  constructor(protected service: NbAuthService,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    protected cd: ChangeDetectorRef,
    protected router: Router,
    protected tokenService: NbTokenService,
    private route: ActivatedRoute) {
    super(service, options, cd, router);
  }
  ngOnInit(): void {
    this.tokenService.clear() ;
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  login(): void {
    this.errors = [];
    this.messages = [];
    this.submitted = true;

    this.service.authenticate(this.strategy, this.user).subscribe((result: NbAuthResult) => {
      this.submitted = false;
      if (result.isSuccess()) {
        this.messages = result.getMessages();
      } else {
        this.errors.push(result.getResponse().error);
      }
      const redirect = result.getRedirect();
      if (redirect) {
        setTimeout(() => {
          this.service.getToken().subscribe(res => {
            this.service.onTokenChange()
            .subscribe((token: NbAuthJWTToken) => {
              if (token.isValid()) {
                const tokens = token.getPayload();
                if (!tokens.role.includes('Member')) {
                  return this.router.navigateByUrl('pages/admin/users');
                } else {
                  return this.router.navigateByUrl(this.returnUrl);
                }
              }
            });
        });
        }, this.redirectDelay);
      }
      this.cd.detectChanges();
    }, error => {

    });

  }

}
