import { Component } from '@angular/core';
import {  NbAuthResult, NbRegisterComponent } from '@nebular/auth';

@Component({
  selector: 'ngx-register',
  templateUrl: './register.component.html',
})
export class NgxRegisterComponent extends NbRegisterComponent {

  register(): void {
    this.errors =  [];
    this.messages = [];
    this.submitted = true;

    this.service.register(this.strategy, this.user).subscribe((result: NbAuthResult) => {
      this.submitted = false;

      if (result.isSuccess()) {
        this.messages.push('Registeration has been done successfully. Please check and confirm your Email.');
      } else {
        this.errors.push(result.getResponse().error);
        this.user = {};
      }


    });
  }
}
