/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { getDeepFromObject, NbAuthResult, NbAuthService, NbRequestPasswordComponent, NB_AUTH_OPTIONS } from '@nebular/auth';


@Component({
  selector: 'ngx-request-password-page',

  templateUrl: './request-password.component.html',

})
export class NgxRequestPasswordComponent extends NbRequestPasswordComponent {







  requestPass(): void {
    this.errors  = [];
    this.messages = [];
    this.submitted = true;

    this.service.requestPassword(this.strategy, this.user).subscribe((result: NbAuthResult) => {
      this.submitted = false;
      this.user = {};

      if (result.isSuccess()) {
        this.messages = result.getMessages();
      } else {

        this.errors.push(result.getResponse().error);
      }

    });
  }


}
