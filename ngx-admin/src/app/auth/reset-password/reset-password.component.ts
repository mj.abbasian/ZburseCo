/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NbAuthResult, NbAuthService, NbResetPasswordComponent, NB_AUTH_OPTIONS } from '@nebular/auth';

@Component({
  selector: 'ngx-reset-password-page',
  styleUrls: ['./reset-password.component.scss'],
  templateUrl: './reset-password.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NgxResetPasswordComponent extends NbResetPasswordComponent {

 constructor(protected service: NbAuthService,
  @Inject(NB_AUTH_OPTIONS) protected options = {},
  protected cd: ChangeDetectorRef,
  protected router: Router,
  private route: ActivatedRoute ) {
super(service, options, cd, router);
}
 // tslint:disable-next-line: use-lifecycle-interface
 ngOnInit(): void {
  this.route.queryParams.subscribe((params) => {
    this.user.email = params.email;
    this.user.token = params.token;
  });

 }


  resetPass(): void {

    this.errors  = [];
    this.messages = [];
    this.submitted = true;
    this.service.resetPassword(this.strategy, this.user).subscribe((result: NbAuthResult) => {

      this.submitted = false;
      if (result.isSuccess()) {
        this.messages = result.getMessages();
      } else {
        this.errors.push(result.getResponse().error);
      }

      const redirect = result.getRedirect();
      if (redirect) {
        setTimeout(() => {
          return this.router.navigateByUrl(redirect);
        }, this.redirectDelay);
      }
      this.cd.detectChanges();
    });
  }


}
