import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HTTP_INTERCEPTORS} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError(error => {
                if (error instanceof HttpErrorResponse) {
                    if (error.status === 401) {
                        return throwError(error.statusText);
                    }
                    const applicationError = error.headers.get('Application-Error');
                    if (applicationError) {

                        return throwError(applicationError);
                    }
                    const serverError = error.error;
                   // console.error(serverError);
                    // console.error(typeof serverError);

                    let modalStateErrors = '';
                    if (serverError && typeof serverError === 'object') {
                        // tslint:disable-next-line: forin
                        for (const key in serverError) {
                           // console.log(key);
                            if (key === 'errors') {
                                // console.log(serverError[key]);
                                // tslint:disable-next-line: forin
                                for (const k in serverError[key]) {
                                   // console.log(k);
                                    modalStateErrors +=  serverError[key][k] + '\n';
                                }

                            }
                             if (key === '0') {
                                if (serverError[key]) {
                                    if (serverError[key]['description'] !== undefined)
                                    modalStateErrors += serverError[key]['description'] + '\n';
                                    else
                                    modalStateErrors += serverError[key] + '\n';
                                }
                            }
                        }
                    }
                    return throwError(modalStateErrors || serverError || 'Server Error');
                }
            }),
        );
    }
}

export const ErrorInterceptorProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true,
};
