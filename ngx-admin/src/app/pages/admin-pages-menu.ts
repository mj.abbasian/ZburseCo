import { NbMenuItem } from '@nebular/theme';

export const ADMIN_MENU_ITEMS: NbMenuItem[] = [
    {
        title: 'UserManagement',
        icon: 'edit-2-outline',
        children: [
          {
            title: 'Users',
            link: '/pages/admin/users',
           },
        ],
    },
    {
        title: 'Packages',
        icon: 'edit-2-outline',
        children: [
          {
            title: 'Packages',
            link: '/pages/admin/packages',
           },
        ],
    },
    {
        title: 'Testimonials',
        icon: 'edit-2-outline',
        children: [
          {
            title: 'Testimonials',
            link: '/pages/admin/testimonials',
           },
        ],
    },

    {
      title: 'Option',
      icon: 'edit-2-outline',
      children: [
        {
          title: 'Option',
          link: '/pages/admin/option',
         },
      ],
    },
    {
      title: 'Payment',
      icon: 'edit-2-outline',
      children: [
        {
          title: 'Payments',
          link: '/pages/admin/payments',
         },
      ],
    },
];

