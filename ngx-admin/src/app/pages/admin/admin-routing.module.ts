import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PackageListResolver } from 'app/_resolvers/package-list.resolver';
import { AdminComponent } from './admin.component';
import { OptionComponent } from './option/option.component';
import { PackageComponent } from './package/package.component';
import { PaymentComponent } from './payment/payment.component';
import { TestimonialComponent } from './testimonial/testimonial.component';
import { UserManagementComponent } from './user-management/user-management.component';


const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        children: [
            {path: 'users', component: UserManagementComponent },
            {path: 'packages', component: PackageComponent , resolve: {packages: PackageListResolver}},
            {path: 'testimonials', component: TestimonialComponent},
            {path: 'option', component: OptionComponent},
            {path: 'payments', component: PaymentComponent},
        ],

        // , resolve:{users:UserListResolver}
    },
];
@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule],

})

export class AdminRoutingModule {
}
