import { NgModule } from '@angular/core';
import { AdminComponent } from './admin.component';
import { AdminRoutingModule } from './admin-routing.module';
import { ThemeModule } from 'app/@theme/theme.module';
// tslint:disable-next-line: max-line-length
import { NbActionsModule, NbAlertModule, NbButtonModule,
  NbCardModule, NbCheckboxModule, NbDatepickerModule,
  NbFormFieldModule, NbIconModule, NbInputModule,
   NbSelectModule, NbTreeGridModule, NbUserModule } from '@nebular/theme';
import { FormsRoutingModule } from '../forms/forms-routing.module';
import { FormsModule as ngFormsModule } from '@angular/forms';
import { PackageComponent } from '../admin/package/package.component';
import { TestimonialComponent } from './testimonial/testimonial.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { AdminService } from 'app/_services/admin.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { OptionComponent } from './option/option.component';
import { PackageListResolver } from 'app/_resolvers/package-list.resolver';
import { PaymentComponent } from './payment/payment.component';
@NgModule({
  imports: [
    AdminRoutingModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbCardModule,
    NbDatepickerModule,
    FormsRoutingModule,
    NbSelectModule,
    NbIconModule,
    ngFormsModule,
    NbAlertModule,
    NbTreeGridModule,
    Ng2SmartTableModule,
    NbFormFieldModule,
  ],
  declarations: [AdminComponent,
    PackageComponent,
    TestimonialComponent,
    UserManagementComponent,
    OptionComponent,
    PaymentComponent,
  ],
  providers: [AdminService , PackageListResolver],
})
export class AdminModule { }
