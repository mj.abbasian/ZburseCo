import { Component, OnInit } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { AdminService } from 'app/_services/admin.service';
import {Option} from '../../../_models/option';

@Component({
  selector: 'ngx-option',
  templateUrl: './option.component.html',
  styleUrls: ['./option.component.scss'],
})
export class OptionComponent implements OnInit {
  option: Option = new Option;
  constructor(private adminService: AdminService , private toastrService: NbToastrService) { }

  ngOnInit() {
    this.getOption();
  }

  getOption() {
    this.adminService.getOption().subscribe((res: Option) => {
      this.option = res;
    } , error => {
      this.toastrService.danger(error);
    });
  }
  updateOption() {
    this.adminService.updateOption(this.option).subscribe((res: Option) => {
      this.toastrService.success('Option updated successfully');
      this.option = res;
    } , error => {
      this.toastrService.danger(error);
    });
  }

}
