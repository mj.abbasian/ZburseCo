import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { Package } from 'app/_models/Package';
import { AdminService } from 'app/_services/admin.service';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.scss'],
})
export class PackageComponent implements OnInit {
  source: LocalDataSource;
  package: Package = new Package();
  selectedFile: File = null;
  editFlag = false;
  settings = {
    columns: {
      name: {
        title: 'Name',
        type: 'string',
      },
      price: {
        title: 'Price',
        type: 'string',
      },
      link: {
        title: 'Link',
        type: 'string',
      },
      description: {
        title: 'Description',
        type: 'string',
      },
      isActive: {
        title: 'Status',
        type: 'boolean',
        valuePrepareFunction: (data) => {
          if (data) {
            return 'Active';
          }
          return 'Deactive';
        },
      },
      photoUrl: {
        title: 'PhotoUrl',
        filter: false,
        type: 'html',
        valuePrepareFunction: (images) => {
          return `<img  width="100px"  src="${images}" (click)="open()"/>`;
        },
      },
    },
    mode: 'external',
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: false,
      custom: [
        {
          name: 'editAction',
          title: '<i  class="ion-edit" title="Edit"></i>',
        },
      ],
      position: 'right',
    },
  };
  constructor(private http: HttpClient , private adminService: AdminService ,
    private toastrService: NbToastrService , private route: ActivatedRoute ) {}

  ngOnInit() {
    this.route.data.subscribe((resolvers: any) => {
      this.source = resolvers['packages'];
    });
  }
  onSelectFile(fileInput: any) {
    this.selectedFile = <File>fileInput.target.files[0];
  }

  addPackage() {
    if (this.selectedFile === null) {
      this.toastrService.danger(' Please select Image file' );
      return;
    }
    const formData = new FormData();
    formData.append('name', this.package.name);
    formData.append('price', this.package.price.toString());
    formData.append('description', this.package.description);
    formData.append('link', this.package.link);
    formData.append('file', this.selectedFile);
    this.package.file = this.selectedFile;
    this.adminService.addPackage(formData).subscribe((res) => {
      this.package = new Package;
      this.getPackage();
      this.editFlag = false;
      this.selectedFile = null;
      this.toastrService.success('Package Added Successful');
    } , error => {
      this.toastrService.danger(error);
    });
  }

  updatePackage() {
    const formData = new FormData();
    if (this.selectedFile !== null) {
      formData.append('file', this.selectedFile);
    }
    formData.append('name', this.package.name);
    formData.append('price', this.package.price.toString());
    formData.append('description', this.package.description);
    formData.append('link', this.package.link);
    formData.append('id', this.package.id.toString());
    formData.append('isActive', this.package.isActive.toString());
    formData.append('photoUrl', this.package.photoUrl);
    this.adminService.updatePackage(formData).subscribe((res) => {
      this.package = new Package;
      this.getPackage();
      this.editFlag = false;
      this.selectedFile = null;
      this.toastrService.success('Package Updated Successful');
    } , error => {
      this.toastrService.danger(error);
    });
  }
  getPackage() {
    this.adminService.getPackages().subscribe((res: any) => {
      this.source = res;
    });
  }
  onCustom(event) {
      this.package = event.data;
      this.editFlag = true;
  }

}
