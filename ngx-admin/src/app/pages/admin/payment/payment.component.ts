import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { AdminService } from 'app/_services/admin.service';
import { ServerDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {

  source: ServerDataSource;
  settings = {
    columns: {
      firstName: {
        title: 'FirstName',
        type: 'string',
      },
      lastName: {
        title: 'LastName',
        type: 'string',
      },
      amount: {
        title: 'Amount',
        type: 'string',
      },
    },
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: false,
      custom: [],
      position: 'right',
    },

  };

  constructor(private http: HttpClient , private adminService: AdminService ,
    private toastrService: NbToastrService) {
    this.source = new ServerDataSource(http,
      {
        endPoint: '/api/admin/getUserPayments',
        totalKey: 'x-total-items',
        pagerPageKey: 'PageNumber',
        pagerLimitKey: 'pageSize',
      });
  }
  ngOnInit() {}

}
