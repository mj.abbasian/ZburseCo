import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { PaginatedResult } from 'app/_models/pagination';
import { AdminService } from 'app/_services/admin.service';
import { ServerDataSource } from 'ng2-smart-table';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'ngx-testimonial',
  templateUrl: './testimonial.component.html',
  styleUrls: ['./testimonial.component.scss'],
})
export class TestimonialComponent implements OnInit {
  source: ServerDataSource;
  settings = {
    columns: {
      link: {
        title: 'Link',
        type: 'string',
      },
      title: {
        title: 'Title',
        type: 'string',
      },
      description: {
        title: 'Description',
        type: 'string',
      },
    },
    actions: {
      columnTitle: 'Actions',
      add: true,
      edit: true,
      delete: true,
      custom: [],
      position: 'right',
    },
    add: {
      addButtonContent: '<i class="ion-plus"></i>',
      createButtonContent: '<i class="ion-checkmark"></i>',
      cancelButtonContent: '<i class="ion-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="ion-edit"></i>',
      saveButtonContent: '<i class="ion-checkmark"></i>',
      cancelButtonContent: '<i class="ion-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="ion-trash-a"></i>',
      confirmDelete: true,
    },
  };

  constructor(private http: HttpClient , private adminService: AdminService ,
    private toastrService: NbToastrService) {
    this.source = new ServerDataSource(http,
      {
        endPoint: '/api/admin/getAllTestimonials',
        totalKey: 'x-total-items',
        pagerPageKey: 'PageNumber',
        pagerLimitKey: 'pageSize',
      });
  }
  ngOnInit() {}

  deleteRecord(event) {
    this.adminService.removeTestimonial(event.data)
    .subscribe((res: any) => {
      if (res != null && res) {
        this.toastrService.success('Delete' , 'Delete Successful');
        this.source.refresh();
      }
    }, (error) => {

    });
  }

  editRecord(event) {
    this.adminService.editTestimonial(event.newData).subscribe((res: any) => {
      this.toastrService.success('Edit', 'Edit Successful');
      this.source.refresh();
    }, (error) => {
      this.toastrService.danger(error);
    },
    );
  }
  saveRecord(event) {
    this.adminService.addTestimonial(event.newData).subscribe((res: any) => {
     if (res.id != null && res.id !== 0) {
       this.toastrService.success('Add', 'Add Successful');
       this.source.refresh();

     } else {
       this.toastrService.danger('Error');
     }
    }, (error) => {
     this.toastrService.danger(error);
   });
 }








  }






