import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { SmartTableData } from 'app/@core/data/smart-table';
import { AdminService } from 'app/_services/admin.service';
import { LocalDataSource, ServerDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss'],
})
export class UserManagementComponent {
  source: ServerDataSource;
  settings = {
    hideSubHeader: 'true',
    columns: {
      id: {
        title: 'User ID',
        type: 'string',
      },
      userName: {
        title: 'UserName',
        type: 'string',
      },
      roles: {
        title: 'Active Roles',
        type: 'string',

      },
    },
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: true,
      delete: false,
      custom: [],
      position: 'right',
    },
    edit: {
      editButtonContent: '<i class="ion-edit"></i>',
      saveButtonContent: '<i class="ion-checkmark"></i>',
      cancelButtonContent: '<i class="ion-close"></i>',
      confirmSave: true,
    },

  };
  constructor(private http: HttpClient , private adminService: AdminService ,
    private toastrService: NbToastrService) {
    this.source = new ServerDataSource(http,
      {
        endPoint: '/api/admin/getUsers',
        totalKey: 'x-total-items',
        pagerPageKey: 'PageNumber',
        pagerLimitKey: 'pageSize',
      });
  }
  editRecord(event) {
    // this.adminService.editTestimonial(event.newData).subscribe((res:any)=>{
    //   this.toastrService.success('Edit','Edit Successful');
    //   this.source.refresh();
    // },(error) => {
    //   this.toastrService.danger(error);
    // }
    // )
  }

}
