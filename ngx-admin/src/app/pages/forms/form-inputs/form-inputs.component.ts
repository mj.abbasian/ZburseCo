import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbComponentStatus, NbToastrService } from '@nebular/theme';
import { User } from 'app/_models/User';
import { UserService } from 'app/_services/user.service';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'ngx-form-inputs',
  styleUrls: ['./form-inputs.component.scss'],
  templateUrl: './form-inputs.component.html',
})
export class FormInputsComponent implements OnInit {
  @ViewChild('editForm', {static: true}) editForm: NgForm;
  starRate = 2;
  heartRate = 4;
  radioGroupValue = 'This is value 2';
  user: User;
  userToken: any;
  constructor(private route: ActivatedRoute,
    private userService: UserService, private toastrService: NbToastrService, private authService: NbAuthService) {

      this.authService.onTokenChange()
      .subscribe((token: NbAuthJWTToken) => {

        if (token.isValid()) {
          this.userToken = token.getPayload();

        }

      });
     }
  ngOnInit(): void {
    // Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    // Add 'implements OnInit' to the class.
    this.route.data.subscribe(data => {
      this.user = data['user'];
    });
  }
  updateUser() {

    this.userService.updateUser(this.userToken.nameid, this.user).subscribe(next => {
      this.showToast('success', 'Profile updated successfully');

    }, error => {
      this.showToast('warning', error);
    });
  }
  showToast(status: NbComponentStatus, error) {
    this.toastrService.show(status, error, { status });
  }
}
