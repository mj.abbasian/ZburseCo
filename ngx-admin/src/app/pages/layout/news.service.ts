import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { PaymentService } from 'app/_services/payment.service';
import { Package } from 'app/_models/Package';
import { PaymentOutPut } from 'app/_models/paymentOutput';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';

const TOTAL_PAGES = 7;

export class NewsPost {
  title: string;
  link: string;
  creator: string;
  text: string;
}

@Injectable()
export class NewsService {


  constructor(private http: HttpClient) {

  }

  load(page: number, pageSize: number): Observable<NewsPost[]> {
    const startIndex = ((page - 1) % TOTAL_PAGES) * pageSize;

    return this.http
      .get<NewsPost[]>('assets/data/news.json')
      .pipe(
        map(news => news.splice(startIndex, pageSize)),
        delay(1500),
      );
  }

  getPackages(): Observable<any[]> {
    return this.http.get<any[]>('api' + '/public/getPackages');
  }
  paymentCallback(authority: string, success: string): Observable<any> {
    return this.http.post<any>('api' + '/payment/paymentCallback', {authority: authority, success: success});
  }
}
