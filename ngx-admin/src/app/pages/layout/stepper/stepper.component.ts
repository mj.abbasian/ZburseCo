import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbComponentStatus, NbStepperComponent, NbToastrService } from '@nebular/theme';
import { Package } from 'app/_models/Package';
import { PaymentOutPut } from 'app/_models/paymentOutput';
import { PaymentService } from 'app/_services/payment.service';
import { UserService } from 'app/_services/user.service';
import { NewsService } from '../news.service';

@Component({
  selector: 'ngx-stepper',
  templateUrl: 'stepper.component.html',
  styleUrls: ['stepper.component.scss'],
})
export class StepperComponent implements OnInit {
  authority = '';
  status = '';
  firstForm: FormGroup;
  secondForm: FormGroup;
  thirdForm: FormGroup;
  user: any;
  loading = false;
  @ViewChild('stepper') stepper: NbStepperComponent = new NbStepperComponent;
  message: string = '';
  error: boolean = false;
  // tslint:disable-next-line: max-line-length
  constructor(private fb: FormBuilder, private newsService: NewsService, private paymentService: PaymentService, private authService: NbAuthService,
    private toastrService: NbToastrService, private route: ActivatedRoute, private userService: UserService) {
    this.authService.onTokenChange()
    .subscribe((token: NbAuthJWTToken) => {

      if (token.isValid()) {
        this.user = token.getPayload();
      }

    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
     if (params['authority']) {
      this.loading = true;
      this.error = false;
      setTimeout(() => {
        this.stepper.next();
       // this.stepper.next();
      }, 500);
      this.authority = params['authority'];
      this.status = params['success'];
      const po = new PaymentOutPut;
      po.authority = this.authority;
      po.status = this.status;
     this.paymentService.paymentCallback(po).subscribe((res: any) => {
       this.loading = false;
       if (res.status === 100) {
        this.loading = true;
        this.userService.addProduct(this.authority).subscribe((_res) => {
          this.loading = false;
         this.message = 'Payment has been done successfully. Click on the following link to download files.';
         this.error = false;

  }, error => {
    this.loading = false;
   this.message = error;
   this.error = true;
  });
       } else {
        this.error = true;
        // this.message="Payment was unsuccessful. Please try again.";
        this.message = 'Payment has been done successfully. Click on the following link to download files.';
       }
     }, error => {
      this.loading = false;
      this.message = error;
      this.error = true;
     });
     }


    });
    this.firstForm = this.fb.group({
      firstCtrl: ['', Validators.required],
    });

    this.secondForm = this.fb.group({
      secondCtrl: ['', Validators.required],
    });

    this.thirdForm = this.fb.group({
      thirdCtrl: ['', Validators.required],
    });
    this.newsService.getPackages().subscribe((_res: any[]) => {
    });
  }

  onFirstSubmit() {
    this.firstForm.markAsDirty();
  }

  onSecondSubmit() {
    this.secondForm.markAsDirty();
  }

  onThirdSubmit() {
    this.thirdForm.markAsDirty();
  }
  paymentRequest() {

  this.loading = true;
    this.paymentService.paymentRequest(this.user.nameid, 15).subscribe((res: PaymentOutPut) => {
      this.loading = false;
      if (res.code === 100) {
        window.location.href = 'https://gate.yekpay.com/api/payment/start/' + res.authority;
      } else {
        this.showToast('warning', res.description);

      }
    }, _error => {

    });
  }
  showToast(status: NbComponentStatus, error) {
    this.toastrService.show(status, error, { status });
  }
  download() {
    window.location.href = 'https://95.216.56.89:8443/smb/file-manager/download?domainId=1211&currentDir=%2Fhttpdocs%2Ffiles&file=ZBurseCo.zip';
  }
}
