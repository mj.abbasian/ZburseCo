import { Component, OnInit } from '@angular/core';
import { NbAuthService } from '@nebular/auth';
import { ADMIN_MENU_ITEMS } from './admin-pages-menu';

import { MENU_ITEMS } from './pages-menu';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit {
  constructor(private authService: NbAuthService) {}
  menu;
  ngOnInit(): void {
    this.authService.getToken().subscribe(res => {
        if (res.getPayload().role === 'Admin') {
          this.menu = ADMIN_MENU_ITEMS;
        } else {
          this.menu = MENU_ITEMS;
        }
    });
  }
}

