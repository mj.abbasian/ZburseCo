import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { tap } from 'rxjs/operators';

@Injectable()
export class RoleGuard implements CanActivate {

  constructor(private authService: NbAuthService, private router: Router) {
  }

  canActivate() {
    return this.authService.isAuthenticated()
      .pipe(
        tap(authenticated => {
          if (authenticated) {
            this.authService.onTokenChange()
            .subscribe((token: NbAuthJWTToken) => {
              if (token.isValid()) {
                const tokens = token.getPayload();
                if (!tokens.role.includes('Member') && !tokens.role.includes('Admin')) {
                    this.router.navigate(['auth/login']);
                }
              }
            });
          }
        }),
      );
  }
}
