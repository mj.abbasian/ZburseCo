import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NbAuthService, NbAuthJWTToken } from '@nebular/auth';
import {
  NbStepperComponent,
  NbToastrService,
  NbComponentStatus,
  NbDialogService,
} from '@nebular/theme';
import { NewsService } from 'app/pages/layout/news.service';
import { PaymentOutPut } from 'app/_models/paymentOutput';
import { Product } from 'app/_models/product';
import { PaymentService } from 'app/_services/payment.service';
import { PublicService } from 'app/_services/public.service';
import { UserService } from 'app/_services/user.service';
import { ProfileComponent } from '../../profile/profile.component';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-binary-option',
  templateUrl: './binary-option.component.html',
  styleUrls: ['./binary-option.component.scss'],
})
export class BinaryOptionComponent implements OnInit {
  authority = '';
  status = '';
  firstForm: FormGroup;
  secondForm: FormGroup;
  thirdForm: FormGroup;
  user: any;
  loading = false;
  @ViewChild('stepper') stepper: NbStepperComponent = new NbStepperComponent();
  message: string = '';
  error: boolean = false;
  // tslint:disable-next-line: max-line-length
  constructor(
    private fb: FormBuilder,
    private publicService: PublicService,
    private paymentService: PaymentService,
    private authService: NbAuthService,
    private toastrService: NbToastrService,
    private route: ActivatedRoute,
    private userService: UserService,
    private dialogService: NbDialogService,
    private router: Router,
  ) {
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      if (token.isValid()) {
        this.user = token.getPayload();
      }
    });
  }

  ngOnInit() {
    this.userService.getProducts().subscribe((resProducts: Product[]) => {
      if (resProducts.length > 0 && resProducts.filter(x => x.package.id === 15).length > 0) {
        setTimeout(() => {
          this.stepper.next();
          // this.stepper.next();
        }, 500);
        this.error = false;
        this.message =
        'Click on the following link to download files.';
      }
    });
    this.route.queryParams.subscribe((params) => {
      if (params['authority']) {
        this.loading = true;
        this.error = false;
        setTimeout(() => {
          this.stepper.next();
          // this.stepper.next();
        }, 500);
        this.authority = params['authority'];
        this.status = params['success'];
        const po = new PaymentOutPut();
        po.authority = this.authority;
        po.status = this.status;
        this.paymentService.paymentCallback(po).subscribe(
          (res: any) => {
            this.loading = false;
            if (res.status === '100') {
              this.loading = true;
              this.userService.addProduct(this.authority).subscribe(
                (_res) => {
                  this.loading = false;
                  this.message =
                    'Payment has been done successfully. Click on the following link to download files.';
                  this.error = false;
                },
                (error) => {
                  this.loading = false;
                  this.message = error;
                  this.error = true;
                },
              );
            } else {
              this.error = true;
              this.message = 'Payment was unsuccessful. Please try again.';
              // this.message = 'Payment has been done successfully. Click on the following link to download files.';
            }
          },
          (error) => {
            this.loading = false;
            this.message = error;
            this.error = true;
          },
        );
      }
    });
    this.firstForm = this.fb.group({
      firstCtrl: ['', Validators.required],
    });

    this.secondForm = this.fb.group({
      secondCtrl: ['', Validators.required],
    });

    this.thirdForm = this.fb.group({
      thirdCtrl: ['', Validators.required],
    });
    this.publicService.getPackages().subscribe((_res: any[]) => {});
  }

  onFirstSubmit() {
    this.firstForm.markAsDirty();
  }

  onSecondSubmit() {
    this.secondForm.markAsDirty();
  }

  onThirdSubmit() {
    this.thirdForm.markAsDirty();
  }
  paymentRequest() {
    this.loading = true;

    this.paymentService.paymentRequest(this.user.nameid, 15).subscribe(
      (res: PaymentOutPut) => {
        this.loading = false;
        // this.userService.addProductFree(15).subscribe(
        //   (_res) => {
        //     window.location.reload();
        //     this.loading = false;
        //     this.message =
        //       'Payment has been done successfully. Click on the following link to download files.';
        //     this.error = false;
        //   },
        //   (error) => {
        //     this.loading = false;
        //     this.message = error;
        //     this.error = true;
        //   },
        // );


        if (res.code === 100) {
          window.location.href =
            'https://gate.yekpay.com/api/payment/start/' + res.authority;
        } else {
          if (res.code === -1 && res.description === 'The parameters are incomplete') {
            this.showToast('warning', 'Please complete the profile information first');
            // tslint:disable-next-line:max-line-length
            this.router.navigate(['pages/user/package/profileBeforPay'], { queryParams: { returnUrl: this.router.url }});
          } else {
            this.showToast('warning', res.description);
          }

        }
      },
      (_error) => {},
    );
  }
  showToast(status: NbComponentStatus, error) {
    this.toastrService.show(status, error, { status });
  }
  download(id) {
    const list = [
      {id: 1, link: 'http://dl.zburse.co/files/BinaryOption/Season%201-Registration.zip'},
      {id: 2, link: 'http://dl.zburse.co/files/BinaryOption/Season%202-Candle%20Analysis.zip'},
      {id: 3, link: 'http://dl.zburse.co/files/BinaryOption/Season%203-Chart%20Devil.zip'},
      {id: 4, link: 'http://dl.zburse.co/files/BinaryOption/Season%204-Capital%20Management.zip'},
      {id: 5, link: 'http://dl.zburse.co/files/BinaryOption/Season%205-Hidden%20Strategy.zip'},
      {id: 6, link: 'http://dl.zburse.co/files/BinaryOption/Season%206-Red%20Green.zip'},
      {id: 7, link: 'http://dl.zburse.co/files/BinaryOption/Season%207-Monday%20Strategy.zip'},
      {id: 8, link: 'http://dl.zburse.co/files/BinaryOption/Season%208-Stepping%20Strategy.zip'},
    ];

    const link = list.filter(x => x.id === id)[0].link;
    window.location.href =
    link;
  }
}
