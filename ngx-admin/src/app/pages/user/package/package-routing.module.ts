import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MemberEditResolver } from 'app/_resolvers/member-edit.resolver';

import { BinaryOptionComponent } from './binary-option/binary-option.component';
import { ForexComponent } from './forex/forex.component';
import { PackageComponent } from './package.component';
import { ProfileBeforPayComponent } from './profile-befor-pay/profile-befor-pay.component';


const routes: Routes = [
  {
    path: '',
    component: PackageComponent,
    children: [
      {
        path: 'binaryOption',
        component: BinaryOptionComponent
        ,
      },
      {
        path: 'forex',
        component: ForexComponent
        ,
      },
      {
        path: 'profileBeforPay',
        component: ProfileBeforPayComponent
        , resolve: {user: MemberEditResolver},
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class PackageRoutingModule {
}

