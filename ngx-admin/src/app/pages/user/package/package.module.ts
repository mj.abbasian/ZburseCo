import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackageComponent } from './package.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// tslint:disable-next-line: max-line-length
import { NbTabsetModule, NbRouteTabsetModule, NbStepperModule, NbCardModule, NbButtonModule, NbListModule, NbAccordionModule, NbUserModule, NbSpinnerModule, NbAlertModule, NbDialogModule, NbIconModule, NbInputModule, NbActionsModule, NbCheckboxModule, NbDatepickerModule, NbRadioModule, NbSelectModule } from '@nebular/theme';
import { ThemeModule } from 'app/@theme/theme.module';
import { LayoutRoutingModule } from 'app/pages/layout/layout-routing.module';
import { BinaryOptionComponent } from './binary-option/binary-option.component';
import { PackageRoutingModule } from './package-routing.module';
import { ProfileBeforPayComponent } from './profile-befor-pay/profile-befor-pay.component';
import { FormsRoutingModule } from 'app/pages/forms/forms-routing.module';
import { FormsModule as ngFormsModule } from '@angular/forms';
import { MemberEditResolver } from 'app/_resolvers/member-edit.resolver';
import { PaymentService } from 'app/_services/payment.service';
import { PublicService } from 'app/_services/public.service';
import { UserService } from 'app/_services/user.service';
import { ForexComponent } from './forex/forex.component';

@NgModule({
  imports: [
    PackageRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ThemeModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbListModule,
    NbAccordionModule,
    NbUserModule,
    LayoutRoutingModule,
    NbSpinnerModule,
    NbAlertModule,
    NbIconModule,
    NbInputModule,
    NbActionsModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    FormsRoutingModule,
    NbSelectModule,
    ngFormsModule,

  ],
  declarations: [PackageComponent, BinaryOptionComponent, ProfileBeforPayComponent, ForexComponent],
  providers: [MemberEditResolver, PaymentService, PublicService, UserService],
})
export class PackageModule { }
