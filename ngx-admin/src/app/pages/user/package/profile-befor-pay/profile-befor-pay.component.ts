import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NbAuthService, NbAuthJWTToken } from '@nebular/auth';
import { NbComponentStatus, NbToastrService } from '@nebular/theme';
import { PaymentOutPut } from 'app/_models/paymentOutput';
import { User } from 'app/_models/User';
import { PaymentService } from 'app/_services/payment.service';
import { UserService } from 'app/_services/user.service';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-profile-befor-pay',
  templateUrl: './profile-befor-pay.component.html',
  styleUrls: ['./profile-befor-pay.component.scss'],
})
export class ProfileBeforPayComponent implements OnInit {
  @ViewChild('editForm', {static: true}) editForm: NgForm;
  starRate = 2;
  heartRate = 4;
  radioGroupValue = 'This is value 2';
  user: User;
  userToken: any;
  loading = false;
  returnUrl: string;
  packageId = 0;
  constructor(private route: ActivatedRoute,
    private userService: UserService, private toastrService: NbToastrService, private authService: NbAuthService
    , private router: Router, private paymentService: PaymentService) {

      this.authService.onTokenChange()
      .subscribe((token: NbAuthJWTToken) => {

        if (token.isValid()) {
          this.userToken = token.getPayload();

        }

      });
     }
  ngOnInit(): void {
    // Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    // Add 'implements OnInit' to the class.
    this.route.data.subscribe(data => {
      this.user = data['user'];
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  updateUser() {

    this.userService.updateUser(this.userToken.nameid, this.user).subscribe(next => {
      this.showToast('success', 'Profile updated successfully');

      if (this.returnUrl === '/pages/user/package/forex') {
        this.packageId = 18;
      }
      if (this.returnUrl === '/pages/user/package/binaryOption') {
        this.packageId = 15;
      }
      this.paymentRequest();


    }, error => {
      this.showToast('warning', error);
    });
  }
  showToast(status: NbComponentStatus, error) {
    this.toastrService.show(status, error, { status });
  }
  paymentRequest() {
    this.loading = true;

    this.paymentService.paymentRequest(this.userToken.nameid, this.packageId).subscribe(
      (res: PaymentOutPut) => {
        this.loading = false;
        if (res.code === 100) {
          window.location.href =
            'https://gate.yekpay.com/api/payment/start/' + res.authority;
        } else {
          if (res.code === -1 && res.description === 'The parameters are incomplete') {
            this.showToast('warning', 'Please complete the profile information first');
            this.router.navigate(['pages/user/profile']);
          } else {
            this.showToast('warning', res.description);
          }

        }
      },
      (_error) => {},
    );
  }

}
