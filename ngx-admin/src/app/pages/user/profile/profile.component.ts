import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NbAuthService, NbAuthJWTToken } from '@nebular/auth';
import { NbToastrService, NbComponentStatus } from '@nebular/theme';
import { User } from 'app/_models/User';
import { UserService } from 'app/_services/user.service';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  @ViewChild('editForm', {static: true}) editForm: NgForm;
  starRate = 2;
  heartRate = 4;
  radioGroupValue = 'This is value 2';
  user: User;
  userToken: any;
  constructor(private route: ActivatedRoute,
    private userService: UserService, private toastrService: NbToastrService, private authService: NbAuthService
    , private router: Router) {

      this.authService.onTokenChange()
      .subscribe((token: NbAuthJWTToken) => {

        if (token.isValid()) {
          this.userToken = token.getPayload();

        }

      });
     }
  ngOnInit(): void {
    // Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    // Add 'implements OnInit' to the class.
    this.route.data.subscribe(data => {
      this.user = data['user'];
    });

  }
  updateUser() {

    this.userService.updateUser(this.userToken.nameid, this.user).subscribe(next => {
      this.showToast('success', 'Profile updated successfully');
      this.router.navigate(['pages/user/package/binaryOption']);

    }, error => {
      this.showToast('warning', error);
    });
  }
  showToast(status: NbComponentStatus, error) {
    this.toastrService.show(status, error, { status });
  }

}
