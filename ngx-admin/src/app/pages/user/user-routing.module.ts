import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MemberEditResolver } from 'app/_resolvers/member-edit.resolver';
import { ProfileComponent } from './profile/profile.component';
import { UserComponent } from './user.component';

const routes: Routes = [
  {
    path: '',
    component: UserComponent,
    children: [
      {
        path: 'profile',
        component: ProfileComponent
        , resolve: {user: MemberEditResolver},
      },
      {
        path: 'package',
        loadChildren: () => import('./package/package.module')
          .then(m => m.PackageModule),
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class UserRoutingModule {
}

