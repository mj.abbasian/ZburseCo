import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
// tslint:disable-next-line: max-line-length
import { NbInputModule, NbCardModule, NbButtonModule, NbActionsModule, NbUserModule, NbCheckboxModule, NbRadioModule, NbDatepickerModule, NbSelectModule, NbIconModule, NbAlertModule } from '@nebular/theme';
import { ThemeModule } from 'app/@theme/theme.module';
import { FormsRoutingModule } from '../forms/forms-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { FormsModule as ngFormsModule } from '@angular/forms';
import { MemberEditResolver } from 'app/_resolvers/member-edit.resolver';
import { PackageModule } from './package/package.module';
import { UserRoutingModule } from './user-routing.module';
import { PublicService } from 'app/_services/public.service';
import { UserService } from 'app/_services/user.service';

@NgModule({
  imports: [
    UserRoutingModule,
    PackageModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    FormsRoutingModule,
    NbSelectModule,
    NbIconModule,
    ngFormsModule,
    NbAlertModule,
  ],
  declarations: [UserComponent, ProfileComponent],
  providers: [MemberEditResolver, PublicService, UserService],
})
export class UserModule { }
