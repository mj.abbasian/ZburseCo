import {Injectable, Injector} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {NbAuthJWTToken, NbAuthService, NbAuthToken} from '@nebular/auth';
import { switchMap } from 'rxjs/operators';

/**
 * TokenInterceptor
 * @see https://angular.io/guide/http#intercepting-all-requests-or-responses
 */
@Injectable()
export class TokenInterceptor implements HttpInterceptor {


    private tokenService: NbAuthJWTToken;

    constructor(private injector: Injector) {
    }

    // public getToken(): string {
    //     return localStorage.getItem('auth_app_token');
    // }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

       // if (!this.filter(req)) {
            return this.authService.isAuthenticatedOrRefresh()
              .pipe(
                switchMap(authenticated => {
                  if (authenticated) {
                      return this.authService.getToken().pipe(
                        switchMap((token: NbAuthToken) => {
                          const JWT = `Bearer ${token.getValue()}`;
                          req = req.clone({
                            setHeaders: {
                              Authorization: JWT,
                            },
                          });
                          return next.handle(req);
                        }),
                      );
                  } else {
                     // Request is sent to server without authentication so that the client code
                     // receives the 401/403 error and can act as desired ('session expired', redirect to login, aso)
                    return next.handle(req);
                  }
                }),
              );


    }
    protected get authService(): NbAuthService {
        return this.injector.get(NbAuthService);
      }
}
